import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

class CustomProgressCircle extends React.Component {
	constructor(props) {
		super(props);

		this.state = {

		};
	}

  render(){
    return (
      <div className="data-loading">
        <span>{this.props.text}</span><br />
        <CircularProgress size={this.props.size} thickness={this.props.thickness} />
      </div>
    );
  }
}

export default CustomProgressCircle;
