export const admin_data = {
  email: "admin@tutor.boesk.com",
  password: "Mindoflow12"
}
export const course_data = {
  user_courses: [
    {course_name: "Core Mathematics", course_code: "CM201", course_tutor: "Evans Boateng"},
    {course_name: "Elective Mathematics", course_code: "EM301", course_tutor: "Evans Boateng"}
  ],
  all_courses: [
    {course_name: "Core Mathematics", course_code: "CM201", course_tutor: "Evans Boateng"},
    {course_name: "Elective Mathematics", course_code: "EM301", course_tutor: "Evans Boateng"},
    {course_name: "Introduction to Computer Science", course_code: "ICS401", course_tutor: "Kadian Davis"}
  ]
}
export const course_detailed_data = [
  {
    course_name: "Core Mathematics",
    course_code: "CM201",
    sections: [
      {
        title: "Introduction",
        content: "Given an HTML fragment, convert it to an object with two keys; " +
                  "one holding the array of ContentBlock objects, and the other " +
                  "holding a reference to the entityMap."
      },
      {
        title: "Set Theory",
        content: "<i>Bla Bla Bla</i>"
      }
    ]
  },
  {
    course_name: "Elective Mathematics",
    course_code: "EM301",
    sections: [
      {
        title: "Introduction",
        content: "Bla bla bla"
      },
      {
        title: "Differentiation",
        content: "Bla Bla Bla"
      },
      {
        title: "Integration",
        content: "Bla Bla Bla"
      }
    ]
  },
  {
    course_name: "Introduction to Computer Science",
    course_code: "ICS401",
    sections: [
      {
        title: "Introduction",
        content: "Bla bla bla"
      },
      {
        title: "Writing your first computer program",
        content: "Note that the Draft library does not currently provide " +
                  "utilities to convert to and from markdown or markup, " +
                  "since different clients may have different requirements " +
                  "for these formats."
      }
    ]
  }
]
