import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from '../../containers/Login';
import Home from '../../containers/Home';

import '../../css/App.css';

class App extends Component {
  render() {
    return (
        <Router>
          <div>
              <Route exact={true} path="/" component={Login} />
              <Route exact={true} path="/home/my-courses" component={Home} />
              <Route exact={true} path="/home/all-courses" component={Home} />
              <Route exact={true} path="/home/my-articles" component={Home} />
              <Route exact={true} path="/home/all-articles" component={Home} />
              <Route exact={true} path="/home/create-course" component={Home} />
              <Route exact={true} path="/home/create-article" component={Home} />
              <Route exact={true} path="/home/settings" component={Home} />
          </div>
        </Router>
    );
  }
}

export default App;
