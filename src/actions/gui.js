import * as ActionConstants from './constants';

export function clickSidebarItem(item_clicked, is_first_click=true){
  var num = ActionConstants.sidebar_item_nums[item_clicked].num;
  return {
    type: ActionConstants.SIDEBAR_ITEM_CLICKED,
    payload: {
      clicked: item_clicked,
      item_num: num,
      is_first_click: is_first_click
    }
  }
}

export function clickSettingsSidebarItem(item_clicked) {
  var num = ActionConstants.settings_sidebar_item_nums[item_clicked].num;
  return {
    type: ActionConstants.SETTINGS_SIDEBAR_ITEM_CLICKED,
    payload: {
      clicked: item_clicked,
      item_num: num
    }
  }
}

export function activateDialog(active_dialog){
   return {
      type: ActionConstants.CLOSE_OPEN_DIALOG,
      payload: { active_dialog: active_dialog }
   }
}

export function setSnackBarOnOff(is_on, text){
  return {
    type: ActionConstants.CLOSE_OPEN_SNACKBAR,
    payload: { is_on: is_on, text: text }
  }
}

export function showHideSidebarDrawer(hide, is_first_click=false){
  return {
    type: ActionConstants.SHOW_HIDE_SIDEBAR_DRAWER,
    payload: {
      is_shown: hide,
      is_first_click: is_first_click
    }
  }
}

export function setBackendErrorsInForm(error_text){
  return {
    type: ActionConstants.data_items.FORM_ERROR_TEXT_SET,
    payload: { error_text: error_text }
  }
}
