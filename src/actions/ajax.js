import axios from 'axios';
import {isEmpty} from '../util/utility';
import { backend_base_url } from '../settings/backend_settings';
import { roles } from '../settings/string_constants';
import { clickSidebarItem,
         clickSettingsSidebarItem,
         activateDialog,
         setSnackBarOnOff,
         setBackendErrorsInForm
       } from './gui';

import * as ActionConstants from './constants';

function loginLogoutSuccess(data, stage){
  return {
    type: ActionConstants.USER_LOGGED_IN,
    payload: { user_data: data, login_state: stage }
  }
}

export function AuthenticateUser(email, password){
  if (email === '' || password === ''){
    return (dispatch) => {
      dispatch(loginLogoutSuccess({}, ActionConstants.data_fetch_stages.UNAUTHENTICATED));
      dispatch({ type: 'RESET' });
    }
  }
  else{
    var url = backend_base_url + 'authenticate/';
    return (dispatch) => {
      dispatch(loginLogoutSuccess({}, ActionConstants.data_fetch_stages.STARTED));
      axios.post(url, {
            email: email,
            password: password
          })
      		.then(function(response) {
            if (!isEmpty(response.data)){ //Successful login
              dispatch(loginLogoutSuccess(response.data, ActionConstants.data_fetch_stages.AUTHENTICATED));
            }
            else{
              dispatch(loginLogoutSuccess({}, ActionConstants.data_fetch_stages.AUTH_ERROR));
            }
      		})
      		.catch(function(error){
            dispatch(loginLogoutSuccess({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
      			console.log(error);
      		});
    };
  }
}

export function fetchAllCourseData(){
  var url = backend_base_url + 'all_courses/';
  return (dispatch) => {
    dispatch(fetchAllCourseDataSuccess({}, ActionConstants.data_fetch_stages.STARTED));
    axios.get(url)
  		.then(function(response) {
        dispatch(fetchAllCourseDataSuccess(response.data, ActionConstants.data_fetch_stages.ENDED));
  		})
  		.catch(function(error){
        dispatch(fetchAllCourseDataSuccess({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
        console.log(error);
  		});
  };
}

export function fetchUserCourseData(tutor_email){
  var url = backend_base_url + 'user_courses/';
  return (dispatch) => {
    dispatch(fetchUserCourseDataSuccess({}, ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
        course_tutor_email: tutor_email
      })
    	.then(function(response) {
        dispatch(fetchUserCourseDataSuccess(response.data, ActionConstants.data_fetch_stages.ENDED));
  		})
  		.catch(function(error){
        dispatch(fetchUserCourseDataSuccess({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
  			console.log(error);
  		});
  };
}

function fetchAllCourseDataSuccess(data, stage){
  return {
    type: ActionConstants.data_items.ALL_COURSE_DATA_UPDATED,
    payload: { all_courses: data, fetch_state: stage }
  }
}

function fetchUserCourseDataSuccess(data, stage){
  return {
    type: ActionConstants.data_items.USER_COURSE_DATA_UPDATED,
    payload: { user_courses: data, fetch_state: stage }
  }
}

export function fetchAdministratorsData(){
  var url = backend_base_url + 'admins/';
  return (dispatch) => {
    dispatch(clickSettingsSidebarItem(ActionConstants.settings_sidebar_items.ADMINISTRATORS));
    dispatch(fetchAdministratorsDataSuccess({}, ActionConstants.data_fetch_stages.STARTED));
    axios.get(url)
    		.then(function(response) {
          dispatch(fetchAdministratorsDataSuccess(response.data, ActionConstants.data_fetch_stages.ENDED));
    		})
    		.catch(function(error){
          dispatch(fetchAdministratorsDataSuccess({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
    			console.log(error);
    		});
  };
}

export function fetchTutorsData(){
  var url = backend_base_url + 'tutors/';
  return (dispatch) => {
    dispatch(clickSettingsSidebarItem(ActionConstants.settings_sidebar_items.TUTORS));
    dispatch(fetchTutorsDataSuccess({}, ActionConstants.data_fetch_stages.STARTED));
    axios.get(url)
    		.then(function(response) {
          dispatch(fetchTutorsDataSuccess(response.data, ActionConstants.data_fetch_stages.ENDED));
    		})
    		.catch(function(error){
          dispatch(fetchTutorsDataSuccess({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
    			console.log(error);
    		});
  };
}

function fetchAdministratorsDataSuccess(data, stage){
  return {
    type: ActionConstants.data_items.ADMIN_DATA_UPDATED,
    payload: { admin_data: data, fetch_state: stage }
  }
}

function fetchTutorsDataSuccess(data, stage){
  return {
    type: ActionConstants.data_items.TUTOR_DATA_UPDATED,
    payload: { tutor_data: data, fetch_state: stage }
  }
}

function createCourseSuccess(stage){
  return {
    type: ActionConstants.data_items.COURSE_CREATED,
    payload: { create_state: stage }
  }
}

export function createCourse(course_name, course_code, course_tutor_email, course_category, course_status, update=false){
  var url = backend_base_url + 'create_update_course/';
  if (update){
    url = backend_base_url + 'create_update_course/update/'; //Update
  }
  return (dispatch, getState) => {
    dispatch(createCourseSuccess(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url,{
        course_name: course_name,
        course_code: course_code,
        course_tutor_email: course_tutor_email,
        course_category: course_category,
        course_status: course_status
      })
      .then(function(response) {
        dispatch(createCourseSuccess(ActionConstants.data_fetch_stages.ENDED));
        const {sidebar} = getState();
        if (sidebar.clicked === ActionConstants.sidebar_items.MY_COURSES){
          const {login_logout} = getState();
          dispatch(fetchUserCourseData(login_logout.user_data.email));
        }
        else {
          dispatch(fetchAllCourseData());
        }
        if (update){
          dispatch(activateDialog(ActionConstants.dialog_names.NONE));
          dispatch(setSnackBarOnOff(true, "Course: " + course_name + " updated"));
        }
        else {
          dispatch(clickSidebarItem(ActionConstants.sidebar_items.MY_COURSES));
        }
      })
      .catch(function(error){
        dispatch(createCourseSuccess(ActionConstants.data_fetch_stages.AJAX_ERROR));
        if (error.response.data['course_code']) { //Duplicate course code
          dispatch(setBackendErrorsInForm(error.response.data['course_code'][0]));
        }
      });
  };
}

function createCourseSectionSuccess(stage){
  return {
    type: ActionConstants.data_items.COURSE_SECTION_CREATED,
    payload: { section_create_state: stage }
  }
}

export function createCourseSection(course_code, section_title, section_content, section_status, section_pos=-1){
  section_content = JSON.stringify(section_content);
  var url = backend_base_url + 'create_update_course_section/';
  var created_updated = " created";
  if (section_pos !== -1){
    url = backend_base_url + 'create_update_course_section/' + section_pos + '/'; //Update
    created_updated = " updated";
  }
  return (dispatch, getState) => {
    dispatch(createCourseSectionSuccess(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url,{
        course_code: course_code,
        section_title: section_title,
        section_content: section_content,
        section_status: section_status
      })
      .then(function(response) {
        dispatch(createCourseSectionSuccess(ActionConstants.data_fetch_stages.ENDED));
        const {sidebar, active_dialog_data} = getState();
        if (sidebar.clicked === ActionConstants.sidebar_items.MY_COURSES){
          const {login_logout} = getState();
          dispatch(fetchUserCourseData(login_logout.user_data.email));
        }
        else {
          dispatch(fetchAllCourseData());
        }
        dispatch(fetchCourseSectionsData(course_code));
        if (active_dialog_data.active_dialog !== ActionConstants.dialog_names.NONE){
          dispatch(activateDialog(ActionConstants.dialog_names.NONE));
          dispatch(setSnackBarOnOff(true, "Course section: " + section_title + created_updated));
        }
      })
      .catch(function(error){
        dispatch(createCourseSectionSuccess(ActionConstants.data_fetch_stages.AJAX_ERROR));
        console.log(error);
      });
  };
}

function createAdminTutorSuccess(stage){
  return {
    type: ActionConstants.data_items.ADMIN_TUTOR_CREATED,
    payload: { create_state: stage }
  }
}
export function createAdminTutor(name, email, password, role, update=false){
  var url = backend_base_url + 'create_update_admin_tutor/';
  var created_updated = " created";
  if (update){
    url = backend_base_url + 'create_update_admin_tutor/update/';
    created_updated = " updated";
  }
  return (dispatch) => {
    dispatch(createAdminTutorSuccess(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
      name: name,
      email: email,
      password: password,
      role: role
    })
    .then(function(response) {
      dispatch(createAdminTutorSuccess(ActionConstants.data_fetch_stages.ENDED));
      if (role === roles.ADMIN){
        dispatch(fetchAdministratorsData());
      }
      else{
        dispatch(fetchTutorsData());
      }
      dispatch(activateDialog(ActionConstants.dialog_names.NONE));
      dispatch(setSnackBarOnOff(true, role + ": " + name + created_updated));
    })
    .catch(function(error){
      dispatch(createAdminTutorSuccess(ActionConstants.data_fetch_stages.AJAX_ERROR));
      if (error.response.data['email']) { //Duplicate email
        dispatch(setBackendErrorsInForm(error.response.data['email'][0]));
      }
    })
  }
}

function fetchAllUsersSuccess(data, stage){
  return {
    type: ActionConstants.data_items.ALL_USERS_DATA_UPDATED,
    payload: { all_users_data: data, fetch_state: stage }
  }
}

export function fetchAllUsersData(){
  var url = backend_base_url + 'all_users/';
  return (dispatch) => {
    dispatch(fetchAllUsersSuccess({}, ActionConstants.data_fetch_stages.STARTED));
    axios.get(url)
    		.then(function(response) {
          dispatch(fetchAllUsersSuccess(response.data, ActionConstants.data_fetch_stages.ENDED));
    		})
    		.catch(function(error){
          dispatch(fetchAllUsersSuccess({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
    			console.log(error);
    		});
  };
}

function fetchCourseSectionsDataSuccess(data, stage){
  return {
    type: ActionConstants.data_items.COURSE_SECTIONS_DATA_UPDATED,
    payload: { course_sections: data, fetch_state: stage }
  }
}

export function fetchCourseSectionsData(course_code){
  var url = backend_base_url + 'course_sections/';
  return (dispatch) => {
    dispatch(fetchCourseSectionsDataSuccess({}, ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
      course_code: course_code
    })
		.then(function(response) {
      dispatch(fetchCourseSectionsDataSuccess(response.data, ActionConstants.data_fetch_stages.ENDED));
		})
		.catch(function(error){
      dispatch(fetchCourseSectionsDataSuccess({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
			console.log(error);
		});
  };
}

function deleteCourseSuccess(stage){
  return {
    type: ActionConstants.data_items.COURSE_DELETED,
    payload: { fetch_state: stage }
  }
}

export function removeCourse(course_code){
  var url = backend_base_url + 'remove_course/';
  return (dispatch, getState) => {
    dispatch(deleteCourseSuccess(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
      course_code: course_code
    })
		.then(function(response) {
      dispatch(deleteCourseSuccess(ActionConstants.data_fetch_stages.ENDED));
      const {sidebar} = getState();
      if (sidebar.clicked === ActionConstants.sidebar_items.MY_COURSES){
        const {login_logout} = getState();
        dispatch(fetchUserCourseData(login_logout.user_data.email));
      }
      else {
        dispatch(fetchAllCourseData());
      }
		})
		.catch(function(error){
      dispatch(deleteCourseSuccess(ActionConstants.data_fetch_stages.AJAX_ERROR));
			console.log(error);
		});
  };
}

function deleteCourseSectionSuccess(stage){
  return {
    type: ActionConstants.data_items.COURSE_SECTION_DELETED,
    payload: { fetch_state: stage }
  }
}

export function removeCourseSection(course_code, section_pos){
  var url = backend_base_url + 'remove_course_section/';
  return (dispatch, getState) => {
    dispatch(deleteCourseSectionSuccess(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
      course_code: course_code,
      section_pos: section_pos
    })
		.then(function(response) {
      dispatch(deleteCourseSectionSuccess(ActionConstants.data_fetch_stages.ENDED));
      const {sidebar} = getState();
      if (sidebar.clicked === ActionConstants.sidebar_items.MY_COURSES){
        const {login_logout} = getState();
        dispatch(fetchUserCourseData(login_logout.user_data.email));
      }
      else {
        dispatch(fetchAllCourseData());
      }
      dispatch(fetchCourseSectionsData(course_code));
		})
		.catch(function(error){
      dispatch(deleteCourseSectionSuccess(ActionConstants.data_fetch_stages.AJAX_ERROR));
			console.log(error);
		});
  };
}

export function removeAdminTutor(admin_tutor){
  var url = backend_base_url + 'remove_admin_tutor/';
  return (dispatch) => {
    dispatch(deleteAdminTutorSuccess(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
      email: admin_tutor.email
    })
		.then(function(response) {
      dispatch(deleteAdminTutorSuccess(ActionConstants.data_fetch_stages.ENDED));
      if (admin_tutor.role === roles.ADMIN){
        dispatch(fetchAdministratorsData());
      }
      else{
        dispatch(fetchTutorsData());
      }
		})
		.catch(function(error){
      dispatch(deleteAdminTutorSuccess(ActionConstants.data_fetch_stages.AJAX_ERROR));
			console.log(error);
		});
  };
}

function deleteAdminTutorSuccess(stage){
  return {
    type: ActionConstants.data_items.ADMIN_TUTOR_DELETED,
    payload: { fetch_state: stage }
  }
}

function setArticleCreationUpdateStatus(stage){
  return {
    type: ActionConstants.data_items.ARTICLE_CREATE_UPDATE,
    payload: { stage: stage }
  }
}

export function createUpdateArticle(code, title, content, author_email, article_status, update=false){
  content = JSON.stringify(content);
  var url = backend_base_url + 'create_update_article/';
  var created_updated = " created";
  if (update){
    url = backend_base_url + 'create_update_article/update/'; //Update
    created_updated = " updated";
  }
  return (dispatch, getState) => {
    dispatch(setArticleCreationUpdateStatus(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url,{
        article_code: code,
        article_title: title,
        article_content: content,
        author_email: author_email,
        article_status: article_status
      })
      .then(function(response) {
        dispatch(setArticleCreationUpdateStatus(ActionConstants.data_fetch_stages.ENDED));
        const {sidebar} = getState();
        if (sidebar.clicked === ActionConstants.sidebar_items.MY_ARTICLES){
          const {login_logout} = getState();
          dispatch(fetchUserArticles(login_logout.user_data.email));
        }
        else {
          dispatch(fetchAllArticles());
        }
        if (update){
          dispatch(activateDialog(ActionConstants.dialog_names.NONE));
          dispatch(setSnackBarOnOff(true, "Article: " + title + created_updated));
        }
        else {
          dispatch(clickSidebarItem(ActionConstants.sidebar_items.MY_ARTICLES));
        }
      })
      .catch(function(error){
        dispatch(setArticleCreationUpdateStatus(ActionConstants.data_fetch_stages.AJAX_ERROR));
        console.log(error);
      });
  };
}

function setFetchAllArticlesStatus(data, stage){
  return {
    type: ActionConstants.data_items.ALL_ARTICLES_UPDATE,
    payload: { all_articles: data, stage: stage }
  }
}

export function fetchAllArticles(){
  var url = backend_base_url + 'all_articles/';
  return (dispatch) => {
    dispatch(setFetchAllArticlesStatus({}, ActionConstants.data_fetch_stages.STARTED));
    axios.get(url)
		.then(function(response) {
      dispatch(setFetchAllArticlesStatus(response.data, ActionConstants.data_fetch_stages.ENDED));
		})
		.catch(function(error){
      dispatch(setFetchAllArticlesStatus({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
			console.log(error);
		});
  };
}

function setFetchUserArticlesStatus(data, stage){
  return {
    type: ActionConstants.data_items.USER_ARTICLES_UPDATE,
    payload: { user_articles: data, stage: stage }
  }
}

export function fetchUserArticles(author_email){
  var url = backend_base_url + 'user_articles/';
  return (dispatch) => {
    dispatch(setFetchUserArticlesStatus({}, ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
        author_email: author_email
      })
    	.then(function(response) {
        dispatch(setFetchUserArticlesStatus(response.data, ActionConstants.data_fetch_stages.ENDED));
  		})
  		.catch(function(error){
        dispatch(setFetchUserArticlesStatus({}, ActionConstants.data_fetch_stages.AJAX_ERROR));
  			console.log(error);
  		});
  };
}

function setRemoveArticleStatus(stage){
  return {
    type: ActionConstants.data_items.ARTICLE_DELETE,
    payload: { stage: stage }
  }
}

export function removeArticle(article_code){
  var url = backend_base_url + 'remove_article/';
  return (dispatch, getState) => {
    dispatch(setRemoveArticleStatus(ActionConstants.data_fetch_stages.STARTED));
    axios.post(url, {
      article_code: article_code
    })
		.then(function(response) {
      dispatch(setRemoveArticleStatus(ActionConstants.data_fetch_stages.ENDED));
      const {sidebar} = getState();
      if (sidebar.clicked === ActionConstants.sidebar_items.MY_ARTICLES){
        const {login_logout} = getState();
        dispatch(fetchUserArticles(login_logout.user_data.email));
      }
      else {
        dispatch(fetchAllArticles());
      }
		})
		.catch(function(error){
      dispatch(setRemoveArticleStatus(ActionConstants.data_fetch_stages.AJAX_ERROR));
			console.log(error);
		});
  };
}
