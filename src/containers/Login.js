import React, { Component } from 'react';
import { RaisedButton, TextField } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {validate_pass, validateEmail} from '../util/manipulate_string';
import TopMenu from './TopMenu';
import {
         data_fetch_stages
       } from '../actions/constants';

import { Redirect } from 'react-router';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AuthenticateUser } from '../actions/ajax';

import '../css/login.css';


const small_btn = {
	fontSize: '12px',
	fontWeight: 'bold',
	textTransform: 'none',
	color: '#fff'
};

const text_field = {
	color: '#000',
	fontSize: '12px'
};

const hint_style = {
	color: '#CCCCCC',
	fontSize: '12px'
};

class Login extends Component {
	constructor(props) {
		super(props);

		this.state = {
      email: '',
      password: '',
      email_err_text: '',
      password_err_text: ''
		};

    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.logUserIn = this.logUserIn.bind(this);
	}

  handleEmail(event) {
		this.setState({email: event.target.value});
	}
  handlePassword(event) {
		this.setState({password: event.target.value});
	}

	logUserIn(){
		const email_value = this.state.email.trim();
		const password_value = this.state.password.trim();
		var email_error = '';
		var pass_error = '';
		var error_found = false;
		/* Check email */
		email_error = validateEmail(email_value);
		if (email_error !== ''){
			error_found = true;
		}
		/* Validate password */
		if (!error_found){
			pass_error = validate_pass(password_value);
		}

		if (email_error === '' &&
				pass_error === ''){
				this.props.AuthenticateUser(email_value, password_value);
		}
		else{
			this.setState({
				email_err_text: email_error,
				password_err_text: pass_error
			});
		}
	}

	render(){
		var pass_err_text = this.state.password_err_text;
		if (this.props.login_logout){
			if (this.props.login_logout.login_state === data_fetch_stages.AUTHENTICATED){
        var redirect_address = '/home/my-courses';
				return <Redirect to={redirect_address} />;
			}
			else if (this.props.login_logout.login_state === data_fetch_stages.AUTH_ERROR){
				pass_err_text = "Email and password cannot be authenticated";
			}
		}

		return (
      <div className="login-page-wrapper">
        <MuiThemeProvider>
    			<div>
    				<TopMenu />
            <div className="login-wrapper">
      					<TextField
      						fullWidth={true}
      						hintText="Email address"
      						inputStyle={ text_field }
      						hintStyle={ hint_style }
      						value={this.state.email}
      						onChange={this.handleEmail}
      						errorText={ this.state.email_err_text }
      					/>
      					<br />
                <TextField
      						fullWidth={true}
      						hintText="Password"
      						inputStyle={ text_field }
      						hintStyle={ hint_style }
                  type="password"
      						value={this.state.password}
      						onChange={this.handlePassword}
      						errorText={ pass_err_text }
      					/>
      					<br />
      					<RaisedButton
      						label="Login"
      						primary={true}
      						labelStyle={ small_btn }
									onClick={ this.logUserIn }
									autoFocus={ true }
									 />
      					<br />
            </div>
    			</div>
        </MuiThemeProvider>
      </div>
		);
	}
}

function mapStateToProps(state){
  return {
		login_logout: state.login_logout,
    sidebar: state.sidebar,
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    AuthenticateUser: AuthenticateUser
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
