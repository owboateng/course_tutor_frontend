import React from 'react';
import CourseList from './CourseList';
import ArticleList from './ArticleList';
import CourseCreator from './CourseCreator';
import CreateArticle from './CreateArticle';
import Settings from './Settings';
import { sidebar_items } from '../actions/constants';

import { connect } from 'react-redux';

import '../css/content.css';

class Content extends React.Component {
	constructor(props) {
		super(props);

		this.state = {

		};
	}

  render(){
		var content_layout = null;
		if (
				!this.props.sidebar ||
				this.props.sidebar.clicked === sidebar_items.MY_COURSES ||
				this.props.sidebar.clicked === sidebar_items.ALL_COURSES
			){
	    content_layout = (
				<div className="content-wrapper">
	      	<CourseList />
				</div>
	    );
		}
		else if (
				!this.props.sidebar ||
				this.props.sidebar.clicked === sidebar_items.MY_ARTICLES ||
				this.props.sidebar.clicked === sidebar_items.ALL_ARTICLES
			){
	    content_layout = (
				<div className="content-wrapper">
	      	<ArticleList />
				</div>
	    );
		}
		else if (this.props.sidebar.clicked === sidebar_items.CREATE_COURSE){
	    content_layout = (
	      <div className="content-wrapper">
					<CourseCreator />
	      </div>
	    );
		}
		else if (this.props.sidebar.clicked === sidebar_items.CREATE_ARTICLE){
	    content_layout = (
	      <div className="content-wrapper">
					<CreateArticle
						article_code=""
						article_title=""
						author_email=""
						editor_state=""
						ctype="new"
						 />
	      </div>
	    );
		}
		else if (this.props.sidebar.clicked === sidebar_items.SETTINGS){
			content_layout = (
	      <div className="content-wrapper">
					<Settings />
	      </div>
	    );
		}

		return content_layout;
  }
}

function mapStateToProps(state){
  return {
		sidebar: state.sidebar,
	}
}

export default connect(mapStateToProps)(Content);
