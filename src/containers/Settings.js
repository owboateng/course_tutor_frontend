import React from 'react';
import { List, ListItem, makeSelectable } from 'material-ui/List';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ActionPermIdentity from 'material-ui/svg-icons/action/perm-identity';
import ActionSupervisorAccount from 'material-ui/svg-icons/action/supervisor-account';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchAdministratorsData,
         fetchTutorsData,
      } from '../actions/ajax';
import { clickSettingsSidebarItem
     } from '../actions/gui';
import { iconstyle, listitemstyle,
         innertextstyle, sidebar_first_style
       } from '../css/inline_consts';
import AdminTutorList from './AdminTutorList';
import '../css/settings.css';
import '../css/progress_bar.css';

let SelectableList = makeSelectable(List);

function wrapState(ComposedComponent) {
  class SelectableList extends React.Component {

    static propTypes = {
      children: PropTypes.node.isRequired,
      defaultValue: PropTypes.number.isRequired,
    };

		componentWillMount() {
      this.props.fetchAdministratorsData();
	 	}
    render() {
      return (
        <ComposedComponent
          value={this.props.settings_sidebar.item_num}
        >
          {this.props.children}
        </ComposedComponent>
      );
    }
  };
	return connect(mapStateToProps, mapDispatchToProps)(SelectableList);
}

SelectableList = wrapState(SelectableList);

class Settings extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
  		rehydrated: false
		}
	}

  render(){
    var right_side = <div className="data-loading">
                      <AdminTutorList />
                     </div>;
    return (
      <div className="">
        <MuiThemeProvider>
          <div>
            <div className="settings-sidebar">
              <SelectableList defaultValue={1} >
                <ListItem primaryText="Administrators"
									value={1}
									style={ Object.assign({}, listitemstyle, sidebar_first_style) }
									innerDivStyle={ innertextstyle }
									leftIcon={<ActionSupervisorAccount  style={ iconstyle }/>}
                  onClick={() => this.props.fetchAdministratorsData() }
                />
                <ListItem primaryText="Tutors"
									value={2}
									style={ listitemstyle }
									innerDivStyle={ innertextstyle }
									leftIcon={<ActionPermIdentity style={ iconstyle }/>}
                  onClick={() => this.props.fetchTutorsData() }
                />
              </SelectableList>
            </div>
            <div className="settings-right-side">
              { right_side }
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
		settings_sidebar: state.settings_sidebar,
    login_logout: state.login_logout
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    fetchAdministratorsData: fetchAdministratorsData,
    fetchTutorsData: fetchTutorsData,
    clickSettingsSidebarItem: clickSettingsSidebarItem
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
