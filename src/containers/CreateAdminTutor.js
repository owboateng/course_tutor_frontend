import React, { Component } from 'react';
import { RaisedButton, TextField } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { alpha, validateEmail, validate_pass } from '../util/manipulate_string';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createAdminTutor } from '../actions/ajax';
import { setBackendErrorsInForm } from '../actions/gui';

const small_btn = {
	fontSize: '12px',
	fontWeight: 'bold',
	textTransform: 'none',
	color: '#fff'
};

const text_field = {
	color: '#000',
	fontSize: '12px'
}
const hint_style = {
	color: '#CCCCCC',
	fontSize: '12px'
}

class CreateAdminTutor extends Component {
	constructor(props) {
		super(props);

		this.state = {
			admin_tutor_name: props.name,
			email: props.email,
			password: '',
			name_err_text: '',
			email_err_text: '',
			pass_err_text: '',
		};

		this.createAdmin = this.createAdmin.bind(this);
		this.handleNameChange = this.handleNameChange.bind(this);
		this.handleEmailChange = this.handleEmailChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
	}

	componentWillMount() {
		this.props.setBackendErrorsInForm('');
	}

	handleNameChange(event) {
		this.setState({admin_tutor_name: event.target.value});
	}

	handleEmailChange(event) {
		this.setState({email: event.target.value});
	}

	handlePasswordChange(event) {
		this.setState({password: event.target.value});
	}

	createAdmin(){
		const admin_tutor_name = this.state.admin_tutor_name.trim();
		const email = this.state.email.trim();
		const password = this.state.password.trim();
		var name_error = '';
		var email_error = '';
		var pass_error = '';
		var error_found = false;
		/* Check name */
    var names = admin_tutor_name.split(" ");
		if (names.length < 2 || names.length > 3){
			name_error =  'Name should be at least two and at most 3';
			error_found = true;
		}

		if (!error_found){
			for (var i=0; i<names.length; i++){
				var name = names[i];
				if (!alpha(name)){
					name_error =  'Name should be alphabets only';
					error_found = true;
					break;
				}
			}
		}

		/* Check email */
		if (!error_found){
			email_error = validateEmail(email);
			if (email_error !== ''){
				error_found = true;
			}
		}

		/* Check password */
		if (!error_found){
			pass_error = validate_pass(password);
		}

		if (name_error === '' && email_error === '' && pass_error === ''){
			if (this.props.ctype === "new"){
				this.props.createAdminTutor(admin_tutor_name, email, password, this.props.role);
			}
			else{
				this.props.createAdminTutor(admin_tutor_name, email, password, this.props.role, true);
			}
		}
		this.setState({
			name_err_text: name_error,
			email_err_text: email_error,
			password_err_text: pass_error
		});
	}

	render(){
		var create_button = '';
		var pass_disabled = false;
		if (this.props.ctype === "new"){
			create_button = <RaisedButton
												label="Add"
												primary={true}
												labelStyle={ small_btn }
												onClick={ this.createAdmin } />;
		}
		else if (this.props.ctype === "edit"){
			pass_disabled = true;
			create_button = <RaisedButton
												label="Save"
												primary={true}
												labelStyle={ small_btn }
												onClick={ this.createAdmin } />;
		}

		return (
			<div className="create-course-wrapper">
				<MuiThemeProvider>
					<div>
						<TextField
							fullWidth={true}
							hintText="Full name"
							inputStyle={ text_field }
							hintStyle={ hint_style }
							value={ this.state.admin_tutor_name }
							onChange={ this.handleNameChange }
							errorText={ this.state.name_err_text }
						/>
						<br />
						<TextField
							fullWidth={true}
							hintText="Email address"
							inputStyle={ text_field }
							hintStyle={ hint_style }
							disabled={ pass_disabled }
							value={ this.state.email }
							onChange={ this.handleEmailChange }
							errorText={ this.state.email_err_text }
						/>
						<br />
						<TextField
							fullWidth={true}
							hintText="Password"
							inputStyle={ text_field }
							hintStyle={ hint_style }
							type="password"
							value={this.state.password}
							onChange={this.handlePasswordChange}
							errorText={ this.state.password_err_text }
						/>
						<br />
						<div className="content-error">{this.props.active_form_error.error_text}</div>
						<br />
						{ create_button }
						<br />
					</div>
				</MuiThemeProvider>
			</div>
		);
	}
}
function mapStateToProps(state){
  return {
		active_form_error: state.active_form_error
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
		createAdminTutor: createAdminTutor,
		setBackendErrorsInForm: setBackendErrorsInForm
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAdminTutor)
