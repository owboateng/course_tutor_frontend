import React from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { convertToRaw } from 'draft-js';
import {RaisedButton, TextField } from 'material-ui';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {alphaNumericSpaceDash } from '../util/manipulate_string';
import {createUpdateArticle} from '../actions/ajax';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { backend_base_url } from '../settings/backend_settings';
import { course_section_statuses } from '../settings/string_constants';

import { data_fetch_stages } from '../actions/constants';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import '../css/editor.css';
import '../css/create_article.css';

const toolbarStyle = {
	fontSize: '12px'
};

const text_style = {
	color: '#000',
	width: '95%',
	marginLeft: '2%',
	fontSize: '12px'
}

const hint_style = {
	color: '#CCCCCC',
	fontSize: '12px'
}

const small_btn = {
	fontSize: '12px',
	fontWeight: 'bold',
	textTransform: 'none',
	color: '#fff'
};

class CreateArticle extends React.Component {
	constructor(props) {
		super(props);

    this.state = {
      article_code: props.article_code,
			article_title: props.article_title,
			editorState: props.editor_state,
      author_value: 1,
      author_email: props.author_email,
      code_err_text: '',
			title_err_text: '',
			editorState_err_text: ''
    };

		this.uploadImageCallBack = this.uploadImageCallBack.bind(this);
  }

	componentWillMount() {
		var tutors = this.props.all_users_data.all_users_data;
		if (this.props.ctype === "edit"){
			var i;
			if ((this.props.login_logout.user_data.role === 'Admin')
					&& (this.props.all_users_data.fetch_state === data_fetch_stages.ENDED))
			{
				/* Set course tutor value */
				for (i=0; i<tutors.length; i++){
					if (tutors[i].email === this.props.author_email){
						break;
					}
				}
				if (i<tutors.length){
					this.setState({author_value: i+1});
				}
			}
		}
	}

  handleArticleCode = (event) => {
		this.setState({article_code: event.target.value});
	}

	handleArticleTitle = (event) => {
		this.setState({article_title: event.target.value});
	}

  handleAuthorChange = (event, index, value) => {
		var author_email = this.props.all_users_data.all_users_data[value-1].email;
		this.setState({author_value: value, author_email: author_email});
	}

  onEditorStateChange = (editorState) => {
    this.setState({editorState: editorState});
	}

  renderAuthorListRow(user_data, index){
		index++;
		var row = <MenuItem value={index} primaryText={user_data.name} key={index}/>
		return row;
	}

	getAllUsersList(all_users_data){
		return (
      all_users_data.map((user_data, index) =>
				this.renderAuthorListRow(user_data, index)
		   )
    );
	}

	uploadImageCallBack(file) {
		var url = backend_base_url + 'upload_image/';
	  return new Promise(
	    (resolve, reject) => {
	      const xhr = new XMLHttpRequest();
				xhr.open('POST', url);
	      const data = new FormData(); // eslint-disable-line no-undef
	      data.append('image', file);
	      xhr.send(data);
	      xhr.addEventListener('load', () => {
	        const response = JSON.parse(xhr.responseText);
	        resolve(response);
	      });
	      xhr.addEventListener('error', () => {
	        const error = JSON.parse(xhr.responseText);
	        reject(error);
	      });
	    }
	  );
	}

	createArticle = () => {
    const code = this.state.article_code.trim();
		const title = this.state.article_title.trim();
    var code_error = '';
		var title_error = '';
		var state_error = '';
		var error_found = false;
		var content = '';
		/* Check length */
    if (code.length < 3){
			code_error =  'Article code should be at least 3 characters';
			error_found = true;
		}
    if (!error_found){
  		if (title.length < 2 ){
  			title_error = 'Article title should be at least 2 characters';
  			error_found = true;
  		}
    }

		if (!error_found){
			/* Check alphanumeric */
			if (!alphaNumericSpaceDash(title)){
				title_error =  'Article title should contain only alphabets, numbers, spaces and dashes';
				error_found = true;
			}
		}

		if (!error_found){
			/* Check content */
			content = this.state.editorState === ''? '' : this.state.editorState.getCurrentContent();
			if (content === '' || !content.hasText() || content.getPlainText().length < 20){
				state_error = 'Article content should be at least 20 characters';
				error_found = true;
			}
		}

		if (!error_found){
			content = this.state.editorState.getCurrentContent();
			var art_content = convertToRaw(content);
			if (this.props.ctype === "new"){
				this.props.createUpdateArticle(
					code,
					title,
					art_content,
					this.props.login_logout.user_data.email,
					course_section_statuses.UNPUBLISHED
				);
			}
			else {
				this.props.createUpdateArticle(
					code,
					title,
					art_content,
					this.props.login_logout.user_data.email,
					course_section_statuses.UNPUBLISHED,
					true
				);
			}
		}
		else{
			this.setState({
        code_err_text: code_error,
				title_err_text: title_error,
				editorState_err_text: state_error
			});
		}

	}

  render() {
		var add_button = '';
		var code_disabled = false;
    var creator = '';
		if (this.props.ctype === "new"){
			add_button = <RaisedButton
												label="Create article"
												primary={true}
												labelStyle={ small_btn }
												style={{marginLeft: '2%'}}
												onClick={ this.createArticle } />;
		}
		else if (this.props.ctype === "edit"){
			add_button = <RaisedButton
												label="Update article"
												primary={true}
												labelStyle={ small_btn }
												style={{marginLeft: '2%'}}
												onClick={ this.createArticle } />;
			code_disabled = true;
		}
    if ((this.props.login_logout.user_data.role === 'Admin')
				&& (this.props.all_users_data.fetch_state === data_fetch_stages.ENDED))
		{
			creator = <div className="article-select">
									<SelectField
										floatingLabelText="Author"
										value={this.state.author_value}
										onChange={this.handleAuthorChange}
									>
										{this.getAllUsersList(this.props.all_users_data.all_users_data)}
									</SelectField>
								</div>;
		}
    return(
			<div className="create-article-wrapper">
        <TextField
          fullWidth={true}
          hintText="Article code"
          style={ text_style }
          hintStyle={ hint_style }
					disabled={code_disabled}
          value={this.state.article_code}
          onChange={this.handleArticleCode}
          errorText={ this.state.code_err_text }
        />
				<TextField
					fullWidth={true}
					hintText="Article title"
					style={ text_style }
					hintStyle={ hint_style }
					value={this.state.article_title}
					onChange={this.handleArticleTitle}
					errorText={ this.state.title_err_text }
				/>
        <br />
        { creator }
        <br />
	      <Editor
					toolbar={{options: ['inline', 'blockType', 'image',
															'link', 'list', 'textAlign'],
										image: { uploadCallback: this.uploadImageCallBack}}}
	        wrapperClassName="wrapper-class"
	        editorClassName="editor-class"
	        toolbarClassName="toolbar-class"
	   			toolbarStyle={toolbarStyle}
	        editorState={this.state.editorState}
	        onEditorStateChange={this.onEditorStateChange}
	      />
				<div className="editor-error">{this.state.editorState_err_text}</div>
				<br />
				{ add_button }
				<br />
			</div>
    );
  }
}

function mapStateToProps(state){
  return {
    login_logout: state.login_logout,
		all_users_data: state.all_users_data,
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
		createUpdateArticle: createUpdateArticle
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateArticle)
