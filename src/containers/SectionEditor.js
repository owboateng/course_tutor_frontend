import React from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { convertToRaw } from 'draft-js';
import {RaisedButton, TextField } from 'material-ui';
import {alphaNumericSpaceDash } from '../util/manipulate_string';
import {createCourseSection} from '../actions/ajax';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { backend_base_url } from '../settings/backend_settings';
import { course_section_statuses } from '../settings/string_constants';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import '../css/editor.css';

const toolbarStyle = {
	fontSize: '12px'
};

const text_style = {
	color: '#000',
	width: '95%',
	marginLeft: '2%',
	fontSize: '12px'
}

const hint_style = {
	color: '#CCCCCC',
	fontSize: '12px'
}

const small_btn = {
	fontSize: '12px',
	fontWeight: 'bold',
	textTransform: 'none',
	color: '#fff'
};

class SectionEditor extends React.Component {
	constructor(props) {
		super(props);

    this.state = {
			section_title: props.title,
			editorState: props.editor_state,
			section_title_err_text: '',
			editorState_err_text: ''
    };

		this.handleSectionTitle = this.handleSectionTitle.bind(this);
    this.onEditorStateChange = this.onEditorStateChange.bind(this);
		this.createNewCourseSection = this.createNewCourseSection.bind(this);
		this.uploadImageCallBack = this.uploadImageCallBack.bind(this);
  }

	handleSectionTitle(event) {
		this.setState({section_title: event.target.value});
	}

  onEditorStateChange(editorState){
    this.setState({editorState: editorState});
	}

	uploadImageCallBack(file) {
		var url = backend_base_url + 'upload_image/';
	  return new Promise(
	    (resolve, reject) => {
	      const xhr = new XMLHttpRequest();
				xhr.open('POST', url);
	      const data = new FormData(); // eslint-disable-line no-undef
	      data.append('image', file);
	      xhr.send(data);
	      xhr.addEventListener('load', () => {
	        const response = JSON.parse(xhr.responseText);
	        resolve(response);
	      });
	      xhr.addEventListener('error', () => {
	        const error = JSON.parse(xhr.responseText);
	        reject(error);
	      });
	    }
	  );
	}

	createNewCourseSection(){
		const title = this.state.section_title.trim();
		var title_error = '';
		var state_error = '';
		var error_found = false;
		var content = '';
		/* Check length */
		if (title.length < 2 ){
			title_error = 'Section title should be at least 2 characters';
			error_found = true;
		}

		if (!error_found){
			/* Check alphanumeric */
			if (!alphaNumericSpaceDash(title)){
				title_error =  'Section title should contain only alphabets, numbers, spaces and dashes';
				error_found = true;
			}
		}

		if (!error_found){
			/* Check content */
			content = this.state.editorState === '' ? '' : this.state.editorState.getCurrentContent();
			if (content === '' || !content.hasText() || content.getPlainText().length < 20){
				state_error = 'Section content should be at least 20 characters';
				error_found = true;
			}
		}

		if (title_error === '' && state_error === ''){
			content = this.state.editorState.getCurrentContent();
			var section_content = convertToRaw(content);
			if (this.props.ctype === "new"){
				this.props.createCourseSection(
					this.props.course_code,
					title,
					section_content,
					course_section_statuses.UNPUBLISHED
				);
			}
			else{
				if (this.props.section_pos !== -1){
					this.props.createCourseSection(
						this.props.course_code,
						title,
						section_content,
						course_section_statuses.UNPUBLISHED,
						this.props.section_pos
					);
				}
				else{
					/* Error */
					console.log("Error: Section position not set");
				}
			}
		}
		else{
			this.setState({
				section_title_err_text: title_error,
				editorState_err_text: state_error
			});
		}

	}

  render() {
		var add_button = '';
		if (this.props.ctype === "new"){
			add_button = <RaisedButton
												label="Add section"
												primary={true}
												labelStyle={ small_btn }
												style={{marginLeft: '2%'}}
												onClick={ this.createNewCourseSection } />;
		}
		else if (this.props.ctype === "edit"){
			add_button = <RaisedButton
												label="Save section"
												primary={true}
												labelStyle={ small_btn }
												style={{marginLeft: '2%'}}
												onClick={ this.createNewCourseSection } />;
		}
    return(
			<div>
				<TextField
					fullWidth={true}
					hintText="Section title"
					style={ text_style }
					hintStyle={ hint_style }
					value={this.state.section_title}
					onChange={this.handleSectionTitle}
					errorText={ this.state.section_title_err_text }
				/>
	      <Editor
					toolbar={{options: ['inline', 'blockType', 'image',
															'link', 'list', 'textAlign'],
										image: { uploadCallback: this.uploadImageCallBack}}}
	        wrapperClassName="wrapper-class"
	        editorClassName="editor-class"
	        toolbarClassName="toolbar-class"
	   			toolbarStyle={toolbarStyle}
	        editorState={this.state.editorState}
	        onEditorStateChange={this.onEditorStateChange}
	      />
				<div className="editor-error">{this.state.editorState_err_text}</div>
				<br />
				{ add_button }
				<br />
			</div>
    );
  }
}

function mapStateToProps(state){
  return {

	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
		createCourseSection: createCourseSection
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionEditor)
