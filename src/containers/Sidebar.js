import React from 'react';
import { List, ListItem, makeSelectable } from 'material-ui/List';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ActionList from 'material-ui/svg-icons/action/list';
import ActionReorder from 'material-ui/svg-icons/action/reorder';
import ActionSettings from 'material-ui/svg-icons/action/settings';
import ContentCreate from 'material-ui/svg-icons/content/create';
import { sidebar_items,
			   data_fetch_stages
			 } from '../actions/constants';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchAllCourseData,
         fetchUserCourseData,
         fetchAllUsersData
       } from '../actions/ajax';
import { showHideSidebarDrawer,
         clickSidebarItem
      } from '../actions/gui';
import { home_url_params } from '../settings/string_constants';
import { withRouter } from 'react-router-dom'
import { Redirect } from 'react-router';

import '../css/sidebar.css';

const iconstyle = {
	fill: "#51CBFC"
}
const listitemstyle = {
	color: "#03347D",
	fontSize: "14px"
}
const innertextstyle = {
	paddingLeft: "42px"
}
const sidebar_first_style = {
	marginTop: "20px"
}

let SelectableList = makeSelectable(List);

function wrapState(ComposedComponent) {
  return class SelectableList extends React.Component {

    static propTypes = {
      children: PropTypes.node.isRequired,
      defaultValue: PropTypes.number.isRequired,
    };

		componentWillMount() {
      this.setState({
        selectedIndex: this.props.defaultValue,
      });
    }

		handleRequestChange = (event, index) => {
      this.setState({
        selectedIndex: index,
      });
    };

    render() {
			var selected_item = 1;
			if (this.props.sidebar && this.props.sidebar.item_num){
				selected_item = this.props.sidebar.item_num;
			}
      return (
        <ComposedComponent
					value={selected_item}
					onChange={this.handleRequestChange}
        >
          {this.props.children}
        </ComposedComponent>
      );
    }
  };
}

SelectableList = wrapState(SelectableList);
SelectableList = connect(mapStateToProps, mapDispatchToProps)(SelectableList);


class Sidebar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};

		this.handleSidebarItemClick = this.handleSidebarItemClick.bind(this);
	}

	componentWillMount() {

	}

	componentDidMount(){

	}

	handleSidebarItemClick(item_number){
		switch (item_number){
			case 2:
				this.props.clickSidebarItem(sidebar_items.ALL_COURSES);
				this.props.history.push('/home/all-courses');
				break;
			case 3:
				this.props.clickSidebarItem(sidebar_items.MY_ARTICLES);
				this.props.history.push('/home/my-articles');
				break;
			case 4:
				this.props.clickSidebarItem(sidebar_items.ALL_ARTICLES);
				this.props.history.push('/home/all-articles');
				break;
			case 5:
				this.props.clickSidebarItem(sidebar_items.CREATE_COURSE);
				this.props.history.push('/home/create-course');
				break;
			case 6:
				this.props.clickSidebarItem(sidebar_items.CREATE_ARTICLE);
				this.props.history.push('/home/create-article');
				break;
			case 7:
				this.props.clickSidebarItem(sidebar_items.SETTINGS);
				this.props.history.push('/home/settings');
				break;
			default:
				this.props.clickSidebarItem(sidebar_items.MY_COURSES);
				this.props.history.push('/home/my-courses');
		}
	}

  render(){
		if (this.props.location.pathname.endsWith(home_url_params.CREATE_COURSE)
				&& this.props.sidebar
				&& this.props.sidebar.clicked === sidebar_items.MY_COURSES)
		{
			return <Redirect to='/home/my-courses' />;
		}
		else if (this.props.location.pathname.endsWith(home_url_params.CREATE_ARTICLE)
				&& this.props.sidebar
				&& this.props.sidebar.clicked === sidebar_items.MY_ARTICLES)
		{
			return <Redirect to='/home/my-articles' />;
		}
		var settings_butt = '';
		var selected_item = 1;
		if ((this.props.login_logout.login_state === data_fetch_stages.AUTHENTICATED)
				&&(this.props.login_logout.user_data.role === "Admin")){
				settings_butt =
				<ListItem primaryText="Settings"
					value={7}
					style={ listitemstyle }
					innerDivStyle={ innertextstyle }
					leftIcon={<ActionSettings style={ iconstyle }/>}
					onClick={() => this.handleSidebarItemClick(7)}
				/>;
		}

		if (this.props.sidebar && this.props.sidebar.item_num){
			selected_item = this.props.sidebar.item_num;
		}

    return  (
      <div className="">
        <MuiThemeProvider>
            <div className="sidebar">
              <SelectableList defaultValue={selected_item} >
                <ListItem primaryText="My courses"
									value={1}
									style={ Object.assign({}, listitemstyle, sidebar_first_style) }
									innerDivStyle={ innertextstyle }
									leftIcon={<ActionList  style={ iconstyle }/>}
                  onTouchTap={() => this.handleSidebarItemClick(1) } />
                <ListItem primaryText="All courses"
									value={2}
									style={ listitemstyle }
									innerDivStyle={ innertextstyle }
									leftIcon={<ActionReorder style={ iconstyle }/>}
                  onTouchTap={() => this.handleSidebarItemClick(2) } />
								<ListItem primaryText="My articles"
									value={3}
									style={ Object.assign({}, listitemstyle) }
									innerDivStyle={ innertextstyle }
									leftIcon={<ActionList  style={ iconstyle }/>}
                  onTouchTap={() => this.handleSidebarItemClick(3) } />
								<ListItem primaryText="All articles"
									value={4}
									style={ listitemstyle }
									innerDivStyle={ innertextstyle }
									leftIcon={<ActionReorder style={ iconstyle }/>}
                  onTouchTap={() => this.handleSidebarItemClick(4) } />
                <ListItem primaryText="Create course"
									value={5}
									style={ listitemstyle }
									innerDivStyle={ innertextstyle }
									leftIcon={<ContentCreate style={ iconstyle }/>}
                  onClick={() => this.handleSidebarItemClick(5)} />
								<ListItem primaryText="Create article"
									value={6}
									style={ listitemstyle }
									innerDivStyle={ innertextstyle }
									leftIcon={<ContentCreate style={ iconstyle }/>}
                  onClick={() => this.handleSidebarItemClick(6)} />
                { settings_butt }
              </SelectableList>
            </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
		sidebar: state.sidebar,
		login_logout: state.login_logout,
		sidebar_drawer: state.sidebar_drawer,
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    clickSidebarItem: clickSidebarItem,
		fetchAllCourseData: fetchAllCourseData,
		fetchUserCourseData: fetchUserCourseData,
		fetchAllUsersData: fetchAllUsersData,
		showHideSidebarDrawer: showHideSidebarDrawer,
  }, dispatch);
}

export default (withRouter)(connect(mapStateToProps, mapDispatchToProps)(Sidebar));
