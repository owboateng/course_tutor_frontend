import React from 'react';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import { settings_sidebar_items,
         data_fetch_stages,
         dialog_names
       } from '../actions/constants';
import { fetchAdministratorsData,
         removeAdminTutor
      } from '../actions/ajax';
import { activateDialog,
         setSnackBarOnOff,
     } from '../actions/gui';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CustomProgressCircle from '../components/custom/CustomProgressCircle';
import CreateAdminTutor from './CreateAdminTutor';

import { admin_email } from '../settings/backend_settings';

import '../css/admin_tutor_list.css';

const small_btn = {
	fontSize: '12px',
	fontWeight: 'bold',
	textTransform: 'none',
	color: '#fff'
};

// const text_field = {
// 	color: '#000',
// 	fontSize: '12px'
// }
// const hint_style = {
// 	color: '#CCCCCC',
// 	fontSize: '12px'
// }

const icon_style = {
    width: '18px',
    height: '18px',
    color: '#51CBFC'
}

const edit_dialog_content_style = {
  width: '70%',
};

const edit_dialog_body_style = {
  minHeigt: '200px'
};

const dialog_title_style = {
  fontSize: '12px',
  fontWeight: 'bold'
}

class AdminTutorList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
      add_edit_dialog: {
        add: true, /* add or edit */
        admin_tutor: ''
      },
      simple_dialog: {
        title: '',
        text: '',
        admin_tutor: '',
        open: false
      }
		};

    this.handleSimpleDialogOpen = this.handleSimpleDialogOpen.bind(this);
    this.handleSimpleDialogClose = this.handleSimpleDialogClose.bind(this);
    this.handleSimpleDialogYes = this.handleSimpleDialogYes.bind(this);
    this.handleAdminTutorAddOpen = this.handleAdminTutorAddOpen.bind(this);
    this.handleAdminTutorEditOpen = this.handleAdminTutorEditOpen.bind(this);
    this.handleAdminTutorAddEditClose = this.handleAdminTutorAddEditClose.bind(this);
    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
    this.getAdminTutorAddForm = this.getAdminTutorAddForm.bind(this);
	}
  handleSimpleDialogOpen = (admin_tutor) => {
    this.setState(
      {
        simple_dialog:
        {
          title: "Delete " + admin_tutor.role,
          text: "Are you sure you would like to delete: " + admin_tutor.name + "?",
          admin_tutor: admin_tutor,
          open: true
        }
      });
  };

  handleSimpleDialogClose = () => {
    this.setState({simple_dialog: {title: '', text: '', admin_tutor:'', open: false}});
  };

  handleSimpleDialogYes = () => {
    this.props.removeAdminTutor(this.state.simple_dialog.admin_tutor);
    this.setState({simple_dialog: {title: '', text: '', open: false}});
  };

  handleAdminTutorAddOpen = () => {
    this.setState({add_edit_dialog: {add: true}});
    this.props.activateDialog(dialog_names.ADMIN_TUTOR_LIST_ADD_EDIT);
  }

  handleAdminTutorAddEditClose = () => {
    this.setState({add_edit_dialog: {add: false}});
    this.props.activateDialog(dialog_names.NONE);
  }

  handleAdminTutorEditOpen(admin_tutor){
    this.setState({add_edit_dialog: {add: false, admin_tutor: admin_tutor}});
    this.props.activateDialog(dialog_names.ADMIN_TUTOR_LIST_ADD_EDIT);
  }

  handleSnackbarClose = () => {
    this.props.setSnackBarOnOff(false, '');
  }

  getAdminTutorAddForm(admin_tutor){
    var edit_form;
    var role = 'Tutor';
    var name = '';
    var email = '';
    var password = '';
    var ctype = 'new';
    if (this.props.settings_sidebar.clicked === settings_sidebar_items.ADMINISTRATORS){
      role = 'Admin';
    }
    if (!this.state.add_edit_dialog.add
      && (this.props.active_dialog_data.active_dialog === dialog_names.ADMIN_TUTOR_LIST_ADD_EDIT)){
      name = this.state.add_edit_dialog.admin_tutor.name;
      email = this.state.add_edit_dialog.admin_tutor.email;
      password = this.state.add_edit_dialog.admin_tutor.password;
      ctype = 'edit';
    }
    edit_form = <CreateAdminTutor
                  name={name}
                  email={email}
                  password={password}
                  role={role}
                  ctype={ctype} />;
    return edit_form;
  }

  getAdminTutorList(admins_tutors){
    var list_layout;
    if (admins_tutors.length === 0){
      if (this.props.settings_sidebar.clicked === settings_sidebar_items.ADMINISTRATORS){
        list_layout = <div className="no-data-wrapper">There are no administrators</div>;
      }
      else{
        list_layout = <div className="no-data-wrapper">There are no tutors</div>;
      }
    }
    else{
      list_layout = admins_tutors.map((a_t, index) =>
				this.renderListRow(a_t, index)
      );
    }
    return list_layout;
	}

  getAddButton(){
    var button_label;
    if (this.props.settings_sidebar.clicked === settings_sidebar_items.ADMINISTRATORS){
      button_label = "Add administrator";
    }
    else{
      button_label = "Add tutor";
    }
    return <RaisedButton
                label={button_label}
                primary={true}
                labelStyle={ small_btn }
                onTouchTap={ () => this.handleAdminTutorAddOpen() }
            />;
  }

  renderListRow(admin_tutor, index){
    var edit_delete_active = false;
    if (admin_tutor.email === admin_email){
      edit_delete_active = true;
    }
    return (
      <div className="admin-list-row" key={index}>
        <span className="admin-name-wrapper">{admin_tutor.name}</span>
        <IconButton tooltip="Edit" touch={true} tooltipPosition="bottom-right"
          iconStyle={icon_style} disabled={edit_delete_active}
          onTouchTap={ () => this.handleAdminTutorEditOpen(admin_tutor) } >
          <ContentCreate />
        </IconButton>
        <IconButton tooltip="Delete" touch={true} tooltipPosition="bottom-right"
          iconStyle={icon_style}  disabled={edit_delete_active}
          onTouchTap={ () => this.handleSimpleDialogOpen(admin_tutor) }>
          <ActionDelete />
        </IconButton>
      </div>
    );
  }

  render(){
		var content_layout;
    var add_edit_dialog_label;
    var add_edit_dialog_open = false;
    if (this.state.add_edit_dialog.add){
      add_edit_dialog_label = "Add new ";
    }
    else {
      add_edit_dialog_label = "Edit ";
    }
    if (this.props.settings_sidebar.clicked === settings_sidebar_items.ADMINISTRATORS){
      add_edit_dialog_label += "administrator";
    }
    else{
      add_edit_dialog_label += "tutor";
    }

    if (this.props.active_dialog_data.active_dialog === dialog_names.ADMIN_TUTOR_LIST_ADD_EDIT){
      add_edit_dialog_open = true;
    }

    var snackbar_open = false;
    var snackbar_text = '';
    if (this.props.active_snackbar.is_on){
      snackbar_open = this.props.active_snackbar.is_on;
      snackbar_text = this.props.active_snackbar.text;
    }

    const simple_actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleSimpleDialogClose}
      />,
      <FlatButton
        label="Yes"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleSimpleDialogYes}
      />,
    ];

    const edit_admin_tutor_actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleAdminTutorAddEditClose}
      />
    ];

    var admin_tutor_edit_dialog =
      <Dialog title={add_edit_dialog_label} actions={edit_admin_tutor_actions}
        modal={false} open={add_edit_dialog_open}
        autoDetectWindowHeight={true}
        autoScrollBodyContent={true}
        contentStyle={edit_dialog_content_style}
        bodyStyle={edit_dialog_body_style}
        titleStyle={dialog_title_style}
        onRequestClose={this.handleAdminTutorAddEditClose} >
        { this.getAdminTutorAddForm() }
      </Dialog>;

    var simple_dialog =
      <Dialog title={this.state.simple_dialog.title} actions={simple_actions}
        modal={false} open={this.state.simple_dialog.open}
        titleStyle={dialog_title_style}
        onRequestClose={this.handleSimpleDialogClose} >
        { this.state.simple_dialog.text }
      </Dialog>;

    var snackbar_notification =
      <Snackbar
        open={snackbar_open}
        message={snackbar_text}
        autoHideDuration={4000}
        onRequestClose={this.handleSnackbarClose}
      />;

		if (!this.props.settings_sidebar || (this.props.settings_sidebar.clicked === settings_sidebar_items.ADMINISTRATORS)){
      if (this.props.administrator_data.fetch_state === data_fetch_stages.STARTED){
  			content_layout = (
  	      <CustomProgressCircle
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
      else if (this.props.administrator_data.fetch_state === data_fetch_stages.ENDED){
        content_layout = (
          <div>
            <div className="admin-tutor-add-wrapper">
              { this.getAddButton() }
            </div>
    	      <div className="admins-tutors-wrapper">
              { this.getAdminTutorList(this.props.administrator_data.admin_data) }
            </div>
            { admin_tutor_edit_dialog }
            { simple_dialog }
            { snackbar_notification }
          </div>
  	    );
      }
      else {
        //console.log(this.props.admin_data);
        content_layout = (
  	      <CustomProgressCircle
            text="There might be a problem :("
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
		}
		else if (this.props.settings_sidebar.clicked === settings_sidebar_items.TUTORS){
      if (this.props.tutor_data.fetch_state === data_fetch_stages.STARTED){
  			content_layout = (
  	      <CustomProgressCircle
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
      else if (this.props.tutor_data.fetch_state === data_fetch_stages.ENDED){
        content_layout = (
          <div>
            <div className="admin-tutor-add-wrapper">
              { this.getAddButton() }
            </div>
    	      <div className="admins-tutors-wrapper">
              { this.getAdminTutorList(this.props.tutor_data.tutor_data) }
            </div>
            { admin_tutor_edit_dialog }
            { simple_dialog }
            { snackbar_notification }
          </div>
  	    );
      }
      else {
        content_layout = (
  	      <CustomProgressCircle
            text="There might be a problem :("
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
		}
		return content_layout;
  }
}

function mapStateToProps(state){
  return {
		settings_sidebar: state.settings_sidebar,
    administrator_data: state.administrator_data,
    tutor_data: state.tutor_data,
    active_dialog_data: state.active_dialog_data,
    login_logout: state.login_logout,
    active_snackbar: state.active_snackbar
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    fetchAdministratorsData: fetchAdministratorsData,
    activateDialog: activateDialog,
    setSnackBarOnOff: setSnackBarOnOff,
    removeAdminTutor: removeAdminTutor
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminTutorList);
