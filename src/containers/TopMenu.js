import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import ActionLockOutline from 'material-ui/svg-icons/action/lock-outline';
import ActionLockOpen from 'material-ui/svg-icons/action/lock-open';
import ActionAccountBox from 'material-ui/svg-icons/action/account-box';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Link} from 'react-router-dom';

import { AuthenticateUser } from '../actions/ajax';
import {
         data_fetch_stages
       } from '../actions/constants';

import teachsomebody from '../images/favicon.gif';
import teachsomebody_text from '../images/teach_web.gif';
import '../css/topmenu.css';

const small_btn = {
	fontSize: '16px',
	textTransform: 'none',
	color: '#25AAE1',
  paddingLeft: '0px'
};

class TopMenu extends Component {
	constructor(props) {
		super(props);

		this.state = {

		};
		this.logUserOut = this.logUserOut.bind(this);
	}

	logUserOut(){
		this.props.AuthenticateUser('', '');//Log user out
	}

  render(){
		var login_button_text = "Login";
    var login_logout_icon =
      <ActionLockOutline
        style={{ width:'18px', paddingBottom: '3px'}}
        color="#25AAE1"
      />;
    var account_icon = '';
		// var home_url = "/";
		if (this.props.login_logout && (this.props.login_logout.login_state === data_fetch_stages.AUTHENTICATED)){
			login_button_text = "Logout";
      login_logout_icon =
        <ActionLockOpen
          style={{ width:'18px', paddingBottom: '3px'}}
          color="#25AAE1"
        />;
      account_icon = <ActionAccountBox style={{ marginBottom: '-7px'}}/>;
			// home_url = "/home";
		}
    return (
        <div className="top-menu">
          <Link className="logo-img-text-wrapper"
            to='/home/my-courses'>
            <span className="logo-wrapper">
              <img src={teachsomebody} alt="logo-img"/>
            </span>
            <span className="logo-text-wrapper">
              <img src={teachsomebody_text} alt="logo-text"/>
            </span>
          </Link>
          <span className="username-login-button-wrapper">
            { account_icon }
            <span className="username-wrapper">
              {this.props.login_logout.user_data.name}
            </span>
						<FlatButton className="login-logout-button-wrapper"
							label={ login_button_text }
							primary={true}
							labelStyle={ small_btn }
              style={{marginLeft: '10px'}}
              icon={login_logout_icon}
							onClick={ this.logUserOut }
					 	/>
        	</span>
        </div>
    );
  }
}

function mapStateToProps(state){
  return {
		login_logout: state.login_logout
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    AuthenticateUser: AuthenticateUser
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TopMenu);
