import React from 'react';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentArchive from 'material-ui/svg-icons/content/archive';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import EditorPublish from 'material-ui/svg-icons/editor/publish';
import NavigationExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import NavigationExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import AvLibraryAdd from 'material-ui/svg-icons/av/library-add';
import ActionViewStream from 'material-ui/svg-icons/action/view-stream';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import Divider from 'material-ui/Divider';
import CreateCourse from './CreateCourse';
import SectionEditor from './SectionEditor';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromRaw, convertToRaw, EditorState } from 'draft-js';
import { sidebar_items,
         data_fetch_stages,
         dialog_names
       } from '../actions/constants';
import { fetchCourseSectionsData,
         removeCourseSection,
         removeCourse,
         createCourse,
         createCourseSection
      } from '../actions/ajax';
import { activateDialog,
         setSnackBarOnOff,
     } from '../actions/gui';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CustomProgressCircle from '../components/custom/CustomProgressCircle';
import MediaQuery from 'react-responsive';
import DragSortableList from 'react-drag-sortable'
import { course_section_statuses } from '../settings/string_constants';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import '../css/content.css';
import '../css/course_list.css';

const icon_style = {
    width: '18px',
    height: '18px',
    color: '#51CBFC'
}

// const section_title_style = {
//   textTransform: 'none'
// }

const edit_info_dialog_content_style = {
  width: '70%',
};

const edit_info_dialog_body_style = {
  minHeigt: '200px'
};

const edit_section_dialog_content_style = {
  width: '82%',
  maxHeight: '600px',
  overflow: 'auto'
};

const edit_section_dialog_body_style = {
  minHeigt: '300px'
};

const dialog_title_style = {
  fontSize: '12px',
  fontWeight: 'bold'
}
const which_simple_dialog = {
  NONE: 'NONE',
  DELETE_COURSE: 'DELETE_COURSE',
  DELETE_SECTION: 'DELETE_SECTION',
  PUBLISH_COURSE: 'PUBLISH_COURSE',
  UNPUBLISH_COURSE: 'UNPUBLISH_COURSE',
  PUBLISH_SECTION: 'PUBLISH_SECTION',
  UNPUBLISH_SECTION: 'UNPUBLISH_SECTION'
}

class CourseList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
      expanded: {
        is_expanded: false,
        index: -1
      },
      course_info_edit_dialog: {
        course: ''
      },
      course_section_edit_dialog: {
        course: '',
        section: '',
        add: true /* add section or edit */
      },
      course_section_view_dialog: {
        section: '',
      },
      simple_dialog: {
        title: '',
        text: '',
        open: false,
        which_dialog: which_simple_dialog.NONE,
        course_code: '',
        section_pos: ''
      }
		};

    this.expand = this.expand.bind(this);
    this.collapse = this.collapse.bind(this);
    this.handleDialogOpen = this.handleDialogOpen.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.handleDialogYes = this.handleDialogYes.bind(this);
    this.handleCourseInfoEditOpen = this.handleCourseInfoEditOpen.bind(this);
    this.handleCourseInfoEditClose = this.handleCourseInfoEditClose.bind(this);
    this.handleCourseSectionEditOpen = this.handleCourseSectionEditOpen.bind(this);
    this.handleCourseSectionEditClose = this.handleCourseSectionEditClose.bind(this);
    this.handleCourseSectionViewOpen = this.handleCourseSectionViewOpen.bind(this);
    this.handleCourseSectionEditClose = this.handleCourseSectionEditClose.bind(this);
    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
    this.renderCourseListRow = this.renderCourseListRow.bind(this);
    this.renderCourseSectionRow = this.renderCourseSectionRow.bind(this);
    this.getCourseSectionViewContent = this.getCourseSectionViewContent.bind(this);
    this.sortCourseSections = this.sortCourseSections.bind(this);
    this.getCourseSectionEditorContent = this.getCourseSectionEditorContent.bind(this);
	}

  componentDidMount() {
    /* Close all dialogs */
    this.props.activateDialog(dialog_names.NONE);
  }

  handleDialogOpen = (title, text, which_dialog, course, section) => {
    this.setState(
    {
      simple_dialog:
      {
        title: title,
        text: text,
        open: true,
        which_dialog: which_dialog,
        course: course,
        section: section
      }
    });
  };

  handleDialogClose = () => {
    this.setState(
    {
      simple_dialog:
      {
        title: '',
        text: '' ,
        open: false,
        which_dialog: which_simple_dialog.NONE,
        course: '',
        section: ''
      }
    });
  };

  handleDialogYes = () => {
    var section = '';
    var section_content = '';
    switch (this.state.simple_dialog.which_dialog) {
      case which_simple_dialog.DELETE_SECTION:
        this.props.removeCourseSection(this.state.simple_dialog.course.course_code, this.state.simple_dialog.section.section_pos);
        break;
      case which_simple_dialog.PUBLISH_SECTION:
        section = this.state.simple_dialog.section;
        const sec_state1 = convertFromRaw(JSON.parse(section.section_content));
        section_content = convertToRaw(sec_state1);
        this.props.createCourseSection(
          section.course_code,
          section.section_title,
          section_content,
          course_section_statuses.PUBLISHED,
          section.section_pos
        );
        break;
      case which_simple_dialog.UNPUBLISH_SECTION:
        section = this.state.simple_dialog.section;
        const sec_state2 = convertFromRaw(JSON.parse(section.section_content));
        section_content = convertToRaw(sec_state2);
        this.props.createCourseSection(
          section.course_code,
          section.section_title,
          section_content,
          course_section_statuses.UNPUBLISHED,
          section.section_pos
        );
        break;
      case which_simple_dialog.DELETE_COURSE:
        this.props.removeCourse(this.state.simple_dialog.course.course_code);
        break;
      case which_simple_dialog.PUBLISH_COURSE:
        this.props.createCourse(
          this.state.simple_dialog.course.course_name,
          this.state.simple_dialog.course.course_code,
          this.state.simple_dialog.course.course_tutor_email,
          this.state.simple_dialog.course.course_category,
          course_section_statuses.PUBLISHED,
          true
        );
        break;
      case which_simple_dialog.UNPUBLISH_COURSE:
        this.props.createCourse(
          this.state.simple_dialog.course.course_name,
          this.state.simple_dialog.course.course_code,
          this.state.simple_dialog.course.course_tutor_email,
          this.state.simple_dialog.course.course_category,
          course_section_statuses.UNPUBLISHED,
          true
        );
        break;
      default:
        console.log("Unknown error"); //Should not happen
    }
    this.handleDialogClose();
  };

  handleCourseInfoEditOpen = (course) => {
    this.setState({course_info_edit_dialog: {course: course}});
    this.props.activateDialog(dialog_names.COURSE_INFO_EDIT);
  }

  handleCourseInfoEditClose = () => {
    this.setState({
      course_info_edit_dialog: {
        course: ''
      }
    });
    this.props.activateDialog(dialog_names.NONE);
  }

  handleCourseSectionEditOpen = (course, section, is_add) => {
    this.setState({
      course_section_edit_dialog: {
        add: is_add,
        course: course,
        section: section
      }
    });
    this.props.activateDialog(dialog_names.COURSE_SECTION_ADD_EDIT);

  }

  handleCourseSectionViewOpen = (section) => {
    this.setState({
      course_section_view_dialog: {
        section: section
      }
    });
    this.props.activateDialog(dialog_names.COURSE_SECTION_VIEW);
  }

  handleCourseSectionEditClose = () => {
    this.props.activateDialog(dialog_names.NONE);
  }

  handleCourseSectionViewClose = () => {
    this.props.activateDialog(dialog_names.NONE);
  }

  handleSnackbarClose = () => {
    this.props.setSnackBarOnOff(false, '');
  }

  expand(clicked_index, course){
    this.setState({expanded: {is_expanded: true, index:clicked_index}});
    this.props.fetchCourseSectionsData(course.course_code);
  }

  collapse(clicked_index){
    this.setState({expanded: {is_expanded: false, index:clicked_index}});
  }

  sortCourseSections(sortedList, dropEvent){
    var prev_list = this.props.course_sections_data.course_sections;
    var i;
    for (i=0; i<sortedList.length; i++){
      var key = parseInt(sortedList[i].content.key, 10);
      var section = prev_list[i];
      const sec_state = convertFromRaw(JSON.parse(section.section_content));
      var section_content = convertToRaw(sec_state);
      if (section.section_pos !== key){
        this.props.createCourseSection(
          section.course_code,
          section.section_title,
          section_content,
          section.section_status,
          key
        );
      }
    }
  }

  courseHasPublishedSections(sections){
    var i;
    var has_published_sections = false;
    for (i=0; i<sections.length; i++){
      if (sections[i].section_status === course_section_statuses.PUBLISHED){
        has_published_sections = true;
        break;
      }
    }

    return has_published_sections;
  }

  getCourseSectionEditorContent(section_content){
    const state = convertFromRaw(JSON.parse(section_content));
    return EditorState.createWithContent(state);
  }

  renderCourseSectionRow(course, section, index){
    var layout_butts = '';

    if (this.props.login_logout.login_state === data_fetch_stages.AUTHENTICATED){
      if ((this.props.login_logout.user_data.role === "Admin") ||
          (this.props.login_logout.user_data.email === course.course_tutor_email)){
        var is_publish_disabled = true;
        var is_unpublish_disabled = true;

        if (section.section_status === course_section_statuses.UNPUBLISHED){
          is_publish_disabled = false;
          is_unpublish_disabled = true;
        }
        else if (section.section_status === course_section_statuses.PUBLISHED){
          is_publish_disabled = true;
          is_unpublish_disabled = false;
        }

        layout_butts =
          <span>
            <IconButton tooltip="View section" touch={true} tooltipPosition="bottom-left"
              iconStyle={icon_style}
              onTouchTap={ () => this.handleCourseSectionViewOpen(section) }
            >
              <ActionViewStream />
            </IconButton>
            <IconButton tooltip="Edit section" touch={true} tooltipPosition="bottom-left"
              iconStyle={icon_style}
              onTouchTap={ () => this.handleCourseSectionEditOpen(course, section, false) } >
              <ContentCreate />
            </IconButton>
            <IconButton tooltip="Delete section" touch={true} tooltipPosition="bottom-left"
              iconStyle={icon_style} disabled={is_publish_disabled}
              onTouchTap={ () => this.handleDialogOpen("Delete course section",
                                    "Are you sure you want to delete section: " + section.section_title + "?",
                                    which_simple_dialog.DELETE_SECTION,
                                    course,
                                    section
                                  )}
            >
              <ActionDelete />
            </IconButton>
            <IconButton tooltip="Publish" touch={true} tooltipPosition="bottom-left"
              iconStyle={icon_style} disabled={is_publish_disabled}
              onTouchTap={ () => this.handleDialogOpen("Publish course",
                                  "Are you sure you want to publish section : " + section.section_title + "?",
                                  which_simple_dialog.PUBLISH_SECTION,
                                  course,
                                  section
                                  ) }>
              <EditorPublish />
            </IconButton>
            <IconButton tooltip="Unpublish" touch={true} tooltipPosition="bottom-left"
              iconStyle={icon_style} disabled={is_unpublish_disabled}
              onTouchTap={ () => this.handleDialogOpen("Unpublish course",
                                  "Are you sure you want to unpublish section: " + section.section_title + "?",
                                  which_simple_dialog.UNPUBLISH_SECTION,
                                  course,
                                  section
                                  ) }>
              <ContentArchive />
            </IconButton>
          </span>;
      }
    }
    return (
      <div key={section.section_pos} className="section-list-row-wrapper">
        <span>
          <span className="section-row-text">
            { section.section_title }
          </span>
          { layout_butts }
        </span>
      </div>
    );
  }

  getCourseSectionsLayout(course){
    var course_sections_layout = '';
    if (this.props.course_sections_data){
      if (this.props.course_sections_data.fetch_state === data_fetch_stages.STARTED){
        course_sections_layout = (
  	      <CustomProgressCircle
            text=""
            size={ 10 }
            thickness={ 2 }
          />
  	    );
      }
      else if (this.props.course_sections_data.fetch_state === data_fetch_stages.ENDED){
        if (this.props.course_sections_data.course_sections.length > 0){
          var sections = this.props.course_sections_data.course_sections;
          sections.sort(function(a, b){
            return parseInt(a.section_pos, 10) - parseInt(b.section_pos, 10);
          });
          var layout_list = sections.map((section, index) =>
            this.renderCourseSectionRow(course, section, index)
          );
          var reorder_list = layout_list.map(function(item){
            return {content: item};
          });

          course_sections_layout =
            <DragSortableList
              items={reorder_list}
              onSort={this.sortCourseSections}
              dropBackTransitionDuration={0.3}
              type="vertical"
            />;
        }
        else{
          course_sections_layout = <div className="no-data-wrapper">Course has no sections. Add new sections</div>;
        }
      }
      else{
        course_sections_layout = (
  	      <CustomProgressCircle
            text="There might be a problem loading data..."
            size={ 10 }
            thickness={ 2 }
          />
  	    );
      }
    }

    return course_sections_layout;
  }

  getCourseInfoEditForm(course){
    var edit_form;
    edit_form = <CreateCourse
                  name={course.course_name}
                  code={course.course_code}
                  category={course.course_category}
                  tutor_email={course.course_tutor_email}
                  status={course.course_status}
                  ctype="edit" />;
    return edit_form;
  }

  getCourseSectionEditForm(){
    if (this.props.active_dialog_data.active_dialog !== dialog_names.COURSE_SECTION_ADD_EDIT){
      return "";
    }
    var ctype = "new";
    var title = "";
    var state = "";
    var section_pos = -1;
    if (!this.state.course_section_edit_dialog.add){
      ctype = "edit";
      title = this.state.course_section_edit_dialog.section.section_title;
      var section_content = this.state.course_section_edit_dialog.section.section_content;
      state = this.getCourseSectionEditorContent(section_content);
      section_pos = this.state.course_section_edit_dialog.section.section_pos;
    }
    var edit_form = <SectionEditor
                  title={title}
                  course_code={this.state.course_section_edit_dialog.course.course_code}
                  editor_state={state}
                  section_pos={section_pos}
                  ctype={ctype}
                />;
    return edit_form;
  }

  getCourseSectionViewContent(){
    if (this.props.active_dialog_data.active_dialog !== dialog_names.COURSE_SECTION_VIEW){
      return "";
    }
    var section_view_div_content = '';
    var section_content = this.state.course_section_view_dialog.section.section_content;
    if (section_content && section_content !== ''){
      console.log(section_content);
      const state = convertFromRaw(JSON.parse(section_content));
      const editor_state = EditorState.createWithContent(state);
      section_view_div_content = <Editor
                                  toolbarHidden={ true }
                                  readOnly={ true }
                                  editorState={editor_state} />
    }
    return section_view_div_content;
  }

  getCourseList(courses){
    var course_list = '';
    if (Array.isArray(courses)){
      if (courses.length > 0){
        course_list = courses.map((course, index) =>
    			this.renderCourseListRow(course, index)
    		);
      }
      else{
        var info = "You have not created any courses";
        if (!this.props.sidebar || (this.props.sidebar.clicked === sidebar_items.ALL_COURSES)){
          info = "No course has been created";
        }
        course_list = <div className="no-courses-wrapper">{info}</div>;
      }
    }
    else{
      course_list = this.renderCourseListRow(courses, 0);
    }

    return course_list;
	}

  renderCourseListRow(course, index){
    var expand_collapse_div;
    var section_div;
    var layout_butts = '';
    var expand_div = <IconButton className={index.toString() + '-expand course-list-icon-butt'} tooltip="Show sections"
      touch={true} tooltipPosition="bottom-left"
      iconStyle={icon_style} onClick={ () => this.expand(index, course) }>
        <NavigationExpandMore />
      </IconButton>;
    var collapse_div = <IconButton className={index.toString() + '-collapse'} tooltip="Hide sections"
      touch={true} tooltipPosition="bottom-left"
      iconStyle={icon_style} onClick={ () => this.collapse(index) }>
        <NavigationExpandLess />
      </IconButton>;
    /* Toggle sections */
    if (this.state.expanded.is_expanded){
      if (this.state.expanded.index === index){
        expand_collapse_div = collapse_div;
        section_div = this.getCourseSectionsLayout(course);
      }
      else{
        expand_collapse_div = expand_div;
        section_div = "";
      }
    }
    else {
      expand_collapse_div = expand_div;
      section_div = "";
    }
    if (this.props.login_logout.login_state === data_fetch_stages.AUTHENTICATED){
      if ((this.props.login_logout.user_data.role === "Admin") ||
          (this.props.login_logout.user_data.email === course.course_tutor_email)){
        /* Enable and disable course buttons here */
        var is_delete_disabled = false;
        var is_publish_disabled = true;
        var is_unpublish_disabled = true;
        if (course.sections_length > 0){
          is_delete_disabled = true;
          if (this.props.login_logout.user_data.role === "Admin"){
            if (course.num_published_sections > 0 ) {
              if (course.course_status === course_section_statuses.UNPUBLISHED){
                is_publish_disabled = false;
                is_unpublish_disabled = true;
              }
              else if (course.course_status === course_section_statuses.PUBLISHED){
                is_publish_disabled = true;
                is_unpublish_disabled = false;
              }
            }
          }
        }

        layout_butts = <span className="course-list-row-buttons">
          <IconButton tooltip="Edit course" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style}
            onTouchTap={ () => this.handleCourseInfoEditOpen(course) }>
            <ContentCreate />
          </IconButton>
          <IconButton tooltip="Add section" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style}
            onTouchTap={ () => this.handleCourseSectionEditOpen(course, "", true) }>
            <AvLibraryAdd />
          </IconButton>
          <IconButton tooltip="Delete course" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style} disabled={is_delete_disabled}
            onTouchTap={ () => this.handleDialogOpen("Delete course",
                                  "Are you sure you want to delete course: " + course.course_name + "?",
                                  which_simple_dialog.DELETE_COURSE,
                                  course,
                                  ''
                                  ) }>
            <ActionDelete />
          </IconButton>
          <IconButton tooltip="Publish" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style} disabled={is_publish_disabled}
            onTouchTap={ () => this.handleDialogOpen("Publish course",
                                "Are you sure you want to publish course: " + course.course_name + "?",
                                which_simple_dialog.PUBLISH_COURSE,
                                course,
                                ''
                                ) }>
            <EditorPublish />
          </IconButton>
          <IconButton tooltip="Unpublish" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style} disabled={is_unpublish_disabled}
            onTouchTap={ () => this.handleDialogOpen("Unpublish course",
                                "Are you sure you want to unpublish course: " + course.course_name + "?",
                                which_simple_dialog.UNPUBLISH_COURSE,
                                course,
                                '') }>
            <ContentArchive />
          </IconButton>
          <MediaQuery maxDeviceWidth={600}>
            <Divider
              style={{width:'85%'}}
            />
          </MediaQuery>
        </span>;
      }
    }

    return (
      <div key={index}>
        <div className="course-list-row">
          {expand_collapse_div}
          <span className="course-name-wrapper">{course.course_name} ({course.course_code})</span>
          { layout_butts }
        </div>
        <div className="sections-wrapper">{ section_div }</div>
      </div>
    );
  }

  render(){
		var content_layout;
    const simple_actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleDialogClose}
      />,
      <FlatButton
        label="Yes"
        primary={true}
        keyboardFocused={true}
        onTouchTap={ () => this.handleDialogYes() }
      />,
    ];
    const edit_info_actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleCourseInfoEditClose}
      />
    ];

    const edit_section_actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleCourseSectionEditClose}
      />
    ];

    const view_section_actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.handleCourseSectionViewClose}
      />
    ]

    var section_edit_dialog_title = "";
    if (this.state.course_section_edit_dialog.course){
      if (this.state.course_section_edit_dialog.add){
        section_edit_dialog_title = "Add new course section to " +
                                      this.state.course_section_edit_dialog.course.course_name;
      }
      else{
        section_edit_dialog_title = "Edit course section to " +
                                    this.state.course_section_edit_dialog.course.course_name;
      }
    }

    var course_info_dialog_open = false;
    var course_section_dialog_open = false;
    var course_section_view_dialog_open = false;
    var section_view_dialog_title = '';

    if (this.props.active_dialog_data.active_dialog === dialog_names.COURSE_INFO_EDIT){
      course_info_dialog_open = true;
    }
    else if (this.props.active_dialog_data.active_dialog === dialog_names.COURSE_SECTION_ADD_EDIT){
      course_section_dialog_open = true;
    }
    else if (this.props.active_dialog_data.active_dialog === dialog_names.COURSE_SECTION_VIEW){
      course_section_view_dialog_open = true;
      section_view_dialog_title = this.state.course_section_view_dialog.section.section_title;
    }

    var snackbar_open = false;
    var snackbar_text = '';
    if (this.props.active_snackbar.is_on){
      snackbar_open = this.props.active_snackbar.is_on;
      snackbar_text = this.props.active_snackbar.text;
    }

    var simple_dialog =
      <Dialog title={this.state.simple_dialog.title} actions={simple_actions}
        modal={false} open={this.state.simple_dialog.open}
        titleStyle={dialog_title_style}
        onRequestClose={this.handleDialogClose} >
        { this.state.simple_dialog.text }
      </Dialog>;

    var course_info_edit_dialog =
      <Dialog title="Edit course info" actions={edit_info_actions}
        modal={false} open={course_info_dialog_open}
        autoDetectWindowHeight={true}
        autoScrollBodyContent={true}
        contentStyle={edit_info_dialog_content_style}
        bodyStyle={edit_info_dialog_body_style}
        titleStyle={dialog_title_style}
        onRequestClose={this.handleCourseInfoEditClose} >
        { this.getCourseInfoEditForm(this.state.course_info_edit_dialog.course) }
      </Dialog>;

    var course_section_edit_dialog =
      <Dialog title={section_edit_dialog_title} actions={edit_section_actions}
        modal={false} open={course_section_dialog_open}
        autoDetectWindowHeight={false}
        autoScrollBodyContent={true}
        contentStyle={edit_section_dialog_content_style}
        bodyStyle={edit_section_dialog_body_style}
        titleStyle={dialog_title_style}
        style={{maxHeight: '400px'}}
        onRequestClose={this.handleCourseSectionEditClose} >
        { this.getCourseSectionEditForm() }
      </Dialog>;

    var course_section_view_dialog =
      <Dialog title={section_view_dialog_title} actions={view_section_actions}
        modal={false} open={course_section_view_dialog_open}
        autoDetectWindowHeight={false}
        autoScrollBodyContent={true}
        contentStyle={edit_section_dialog_content_style}
        bodyStyle={edit_section_dialog_body_style}
        titleStyle={dialog_title_style}
        style={{maxHeight: '400px'}}
        onRequestClose={this.handleCourseSectionViewClose} >
        { this.getCourseSectionViewContent() }
      </Dialog>;

    var snackbar_notification =
      <Snackbar
        open={snackbar_open}
        message={snackbar_text}
        autoHideDuration={4000}
        onRequestClose={this.handleSnackbarClose}
      />;

		if (!this.props.sidebar || (this.props.sidebar.clicked === sidebar_items.MY_COURSES)){
      if (this.props.user_course_data.fetch_state === data_fetch_stages.STARTED){
  			content_layout = (
  	      <CustomProgressCircle
            text=""
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
      else if (this.props.user_course_data.fetch_state === data_fetch_stages.ENDED){
        content_layout = (
  	      <div>
            { this.getCourseList(this.props.user_course_data.user_courses) }
            { simple_dialog }
            { course_info_edit_dialog }
            { course_section_edit_dialog }
            { course_section_view_dialog }
            { snackbar_notification }
          </div>
  	    );
      }
      else {
        //console.log(this.props.user_course_data);
        content_layout = (
  	      <CustomProgressCircle
            text="There might be a problem loading data..."
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
		}
		else if (this.props.sidebar.clicked === sidebar_items.ALL_COURSES){
      if (this.props.all_course_data.fetch_state === data_fetch_stages.STARTED){
  			content_layout = (
  	      <CustomProgressCircle
            text=""
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
      else if (this.props.all_course_data.fetch_state === data_fetch_stages.ENDED){
        content_layout = (
  	      <div>
            { this.getCourseList(this.props.all_course_data.all_courses) }
            { simple_dialog }
            { course_info_edit_dialog }
            { course_section_edit_dialog }
            { course_section_view_dialog }
          </div>
  	    );
      }
      else {
        content_layout = (
  	      <CustomProgressCircle
            text="There might be a problem :("
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
		}
		return content_layout;
  }
}

function mapStateToProps(state){
  return {
		sidebar: state.sidebar,
    user_course_data: state.user_course_data,
    all_course_data: state.all_course_data,
    course_sections_data: state.course_sections_data,
    active_dialog_data: state.active_dialog_data,
    login_logout: state.login_logout,
    active_snackbar: state.active_snackbar
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    fetchCourseSectionsData: fetchCourseSectionsData,
    activateDialog: activateDialog,
    setSnackBarOnOff: setSnackBarOnOff,
    removeCourseSection: removeCourseSection,
    removeCourse: removeCourse,
    createCourse: createCourse,
    createCourseSection: createCourseSection
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseList);
