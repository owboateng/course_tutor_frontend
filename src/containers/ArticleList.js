import React from 'react';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentArchive from 'material-ui/svg-icons/content/archive';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import EditorPublish from 'material-ui/svg-icons/editor/publish';
import ActionViewStream from 'material-ui/svg-icons/action/view-stream';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import CreateArticle from './CreateArticle';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromRaw, convertToRaw, EditorState } from 'draft-js';
import { sidebar_items,
         data_fetch_stages,
         dialog_names
       } from '../actions/constants';
import { fetchUserArticles,
         fetchAllArticles,
         removeArticle,
         createUpdateArticle
      } from '../actions/ajax';
import { activateDialog,
         setSnackBarOnOff,
     } from '../actions/gui';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CustomProgressCircle from '../components/custom/CustomProgressCircle';
import MediaQuery from 'react-responsive';
import { course_section_statuses } from '../settings/string_constants';
import { format_article_title } from '../util/manipulate_string';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import '../css/content.css';
import '../css/course_list.css';
import '../css/article_list.css';

const icon_style = {
    width: '18px',
    height: '18px',
    color: '#51CBFC'
}

const edit_section_dialog_content_style = {
  width: '82%',
  maxHeight: '600px',
  overflow: 'auto'
};

const edit_section_dialog_body_style = {
  minHeigt: '300px'
};

const dialog_title_style = {
  fontSize: '12px',
  fontWeight: 'bold'
}

class ArticleList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
      article_dialog: {
        article: ''
      }
		};

    this.renderArticleListRow = this.renderArticleListRow.bind(this);
	}

  componentDidMount() {
    /* Close all dialogs */
    this.props.activateDialog(dialog_names.NONE);
  }

  handleArticleStatusChange = (article, dialog_name) => {
    this.setState({
      article_dialog: {
        article: article
      }
    });
    this.props.activateDialog(dialog_name);
  }

  handleArticleDialogClose = () => {
    this.props.activateDialog(dialog_names.NONE);
  }

  handleDialogYes = () => {
    var article = this.state.article_dialog.article;
    var article_content = '';
    switch (this.props.active_dialog_data.active_dialog) {
      case dialog_names.ARTICLE_REMOVE:
        this.props.removeArticle(this.state.article_dialog.article.article_code);
        break;
      case dialog_names.ARTICLE_PUBLISH:
        const state1 = convertFromRaw(JSON.parse(this.state.article_dialog.article.article_content));
        article_content = convertToRaw(state1);
        this.props.createUpdateArticle(
          article.article_code,
          article.article_title,
          article_content,
          article.author_email,
          course_section_statuses.PUBLISHED,
          true
        );
        break;
      case dialog_names.ARTICLE_UNPUBLISH:
        const state2 = convertFromRaw(JSON.parse(this.state.article_dialog.article.article_content));
        article_content = convertToRaw(state2);
        this.props.createUpdateArticle(
          article.article_code,
          article.article_title,
          article_content,
          article.author_email,
          course_section_statuses.UNPUBLISHED,
          true
        );
        break;
      default:
        console.log("Unknown error"); //Should not happen
    }
    this.props.activateDialog(dialog_names.NONE);
  }

  getArticleViewContent = () => {
    if (this.props.active_dialog_data.active_dialog !== dialog_names.ARTICLE_VIEW){
      return "";
    }
    var article_view_div_content = '';
    var article_content = this.state.article_dialog.article.article_content;
    if (article_content && article_content !== ''){
      console.log(article_content);
      const state = convertFromRaw(JSON.parse(article_content));
      const editor_state = EditorState.createWithContent(state);
      article_view_div_content = <Editor
                                  toolbarHidden={ true }
                                  readOnly={ true }
                                  editorState={editor_state} />
    }
    return article_view_div_content;
  }

  getArticleEditContent = () => {
    if (this.props.active_dialog_data.active_dialog !== dialog_names.ARTICLE_EDIT){
      return "";
    }
    var article_edit_div_content = '';
    var article_content = this.state.article_dialog.article.article_content;
    if (article_content && article_content !== ''){
      const state = convertFromRaw(JSON.parse(article_content));
      const editor_state = EditorState.createWithContent(state);
      article_edit_div_content = <CreateArticle
                                    article_code={this.state.article_dialog.article.article_code}
                                    article_title={this.state.article_dialog.article.article_title}
                                    author_email={this.state.article_dialog.article.author_email}
                                    editor_state={editor_state}
                                    ctype="edit"
                                 />;
    }
    return article_edit_div_content;
  }


  getArticleList(articles){
    var article_list = '';
    if (Array.isArray(articles)){
      if (articles.length > 0){
        article_list = articles.map((article, index) =>
    			this.renderArticleListRow(article, index)
    		);
      }
      else{
        var info = "You have not created any articles";
        if (!this.props.sidebar || (this.props.sidebar.clicked === sidebar_items.ALL_ARTICLES)){
          info = "No articles has been created";
        }
        article_list = <div className="no-courses-wrapper">{info}</div>;
      }
    }
    else{
      article_list = this.renderArticleListRow(articles, 0);
    }

    return article_list;
	}

  renderArticleListRow(article, index){
    var layout_butts = '';

    if (this.props.login_logout.login_state === data_fetch_stages.AUTHENTICATED){
      if ((this.props.login_logout.user_data.role === "Admin") ||
          (this.props.login_logout.user_data.email === article.author_email)){
        /* Enable and disable article buttons here */
        var is_publish_disabled = true;
        var is_unpublish_disabled = true;

        if (article.article_status === course_section_statuses.UNPUBLISHED){
          is_publish_disabled = false;
          is_unpublish_disabled = true;
        }
        else if (article.article_status === course_section_statuses.PUBLISHED){
          is_publish_disabled = true;
          is_unpublish_disabled = false;
        }

        layout_butts = <span className="course-list-row-buttons">
          <IconButton tooltip="View article" touch={true} tooltipPosition="bottom-left"
            iconStyle={icon_style}
            onTouchTap={ () => this.handleArticleStatusChange(article, dialog_names.ARTICLE_VIEW) }
          >
            <ActionViewStream />
          </IconButton>
          <IconButton tooltip="Edit article" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style}
            onTouchTap={ () => this.handleArticleStatusChange(article, dialog_names.ARTICLE_EDIT) }>
            <ContentCreate />
          </IconButton>
          <IconButton tooltip="Delete article" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style} disabled={is_publish_disabled}
            onTouchTap={ () => this.handleArticleStatusChange(article, dialog_names.ARTICLE_REMOVE) }>
            <ActionDelete />
          </IconButton>
          <IconButton tooltip="Publish" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style} disabled={is_publish_disabled}
            onTouchTap={ () => this.handleArticleStatusChange(article, dialog_names.ARTICLE_PUBLISH) }>
            <EditorPublish />
          </IconButton>
          <IconButton tooltip="Unpublish" touch={true} tooltipPosition="bottom-right"
            iconStyle={icon_style} disabled={is_unpublish_disabled}
            onTouchTap={ () => this.handleArticleStatusChange(article, dialog_names.ARTICLE_UNPUBLISH) }>
            <ContentArchive />
          </IconButton>
          <MediaQuery maxDeviceWidth={600}>
            <Divider
              style={{width:'85%'}}
            />
          </MediaQuery>
        </span>;
      }
    }

    return (
      <div key={index}>
        <div className="course-list-row">
          <span className="course-name-wrapper">{format_article_title(article.article_title)} ({article.article_code})</span>
          { layout_butts }
        </div>
      </div>
    );
  }

  render(){
		var content_layout;
    var dialog_title = '';
    var dialog_content = '';
    var dialog_close_label = 'Close';
    var article_dialog_open = false;

    if (this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_VIEW){
      article_dialog_open = true;
      dialog_title = this.state.article_dialog.article.article_title;
      dialog_content = this.getArticleViewContent();
    }
    else if (this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_EDIT){
      article_dialog_open = true;
      dialog_title = 'Edit article';
      dialog_content = this.getArticleEditContent();
      dialog_close_label = 'Cancel';
    }
    else if (this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_REMOVE){
      article_dialog_open = true;
      dialog_title = 'Remove article';
      dialog_content = 'Are you sure that you want to remove: ' + this.state.article_dialog.article.article_title + '?';
    }
    else if (this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_PUBLISH){
      article_dialog_open = true;
      dialog_title = 'Publish article';
      dialog_content = 'Are you sure that you want to publish: ' + this.state.article_dialog.article.article_title + '?';
    }
    else if (this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_UNPUBLISH){
      article_dialog_open = true;
      dialog_title = 'Unpublish article';
      dialog_content = 'Are you sure that you want to unpublish: ' + this.state.article_dialog.article.article_title + '?';
    }
    const cancel_close_action = [
      <FlatButton
        label={dialog_close_label}
        primary={true}
        onTouchTap={this.handleArticleDialogClose}
      />
    ];

    const yes_no_actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleArticleDialogClose}
      />,
      <FlatButton
        label="Yes"
        primary={true}
        keyboardFocused={true}
        onTouchTap={ this.handleDialogYes }
      />,
    ]

    var action_butts = cancel_close_action;
    if (
      this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_REMOVE ||
      this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_PUBLISH ||
      this.props.active_dialog_data.active_dialog === dialog_names.ARTICLE_UNPUBLISH
    )
    {
      action_butts = yes_no_actions;
    }

    var article_dialog =
      <Dialog title={dialog_title} actions={action_butts}
        modal={false} open={article_dialog_open}
        autoDetectWindowHeight={false}
        autoScrollBodyContent={true}
        contentStyle={edit_section_dialog_content_style}
        bodyStyle={edit_section_dialog_body_style}
        titleStyle={dialog_title_style}
        style={{maxHeight: '400px'}}
        onRequestClose={ this.handleArticleDialogClose } >
        { dialog_content }
      </Dialog>;

		if (!this.props.sidebar || (this.props.sidebar.clicked === sidebar_items.MY_ARTICLES)){
      if (this.props.user_articles.stage === data_fetch_stages.STARTED){
  			content_layout = (
  	      <CustomProgressCircle
            text=""
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
      else if (this.props.user_articles.stage === data_fetch_stages.ENDED){
        content_layout = (
  	      <div>
            { this.getArticleList(this.props.user_articles.user_articles) }
            { article_dialog }
          </div>
  	    );
      }
      else {
        content_layout = (
  	      <CustomProgressCircle
            text="There might be a problem loading data..."
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
		}
		else if (this.props.sidebar.clicked === sidebar_items.ALL_ARTICLES){
      if (this.props.all_articles.stage === data_fetch_stages.STARTED){
  			content_layout = (
  	      <CustomProgressCircle
            text=""
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
      else if (this.props.all_articles.stage === data_fetch_stages.ENDED){
        content_layout = (
  	      <div>
            { this.getArticleList(this.props.all_articles.all_articles) }
            { article_dialog }
          </div>
  	    );
      }
      else {
        content_layout = (
  	      <CustomProgressCircle
            text="There might be a problem :("
            size={ 30 }
            thickness={ 5 }
          />
  	    );
      }
		}
		return content_layout;
  }
}

function mapStateToProps(state){
  return {
		sidebar: state.sidebar,
    user_articles: state.user_articles,
    all_articles: state.all_articles,
    active_dialog_data: state.active_dialog_data,
    login_logout: state.login_logout,
    active_snackbar: state.active_snackbar
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    fetchUserArticles: fetchUserArticles,
    fetchAllArticles: fetchAllArticles,
    activateDialog: activateDialog,
    setSnackBarOnOff: setSnackBarOnOff,
    removeArticle: removeArticle,
    createUpdateArticle: createUpdateArticle
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleList);
