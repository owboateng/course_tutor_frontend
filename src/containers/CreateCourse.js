import React, { Component } from 'react';
import { RaisedButton, TextField } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {alphanumeric, alphaNumericSpaceDash} from '../util/manipulate_string';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { createCourse,
				 fetchAllUsersData,
       } from '../actions/ajax';
import { setBackendErrorsInForm,
         clickSidebarItem
      } from '../actions/gui';
import {
         data_fetch_stages
       } from '../actions/constants';
import { roles, course_section_statuses } from '../settings/string_constants';
import { course_categories } from '../constants/course_model_constants';

const small_btn = {
	fontSize: '12px',
	fontWeight: 'bold',
	textTransform: 'none',
	color: '#fff'
};

const text_field = {
	color: '#000',
	fontSize: '12px'
}
const hint_style = {
	color: '#CCCCCC',
	fontSize: '12px'
}

class CreateCourse extends Component {
	constructor(props) {
		super(props);

		this.state = {
			course_name_value: props.name,
			course_code_value: props.code,
			selected_tutor_email: props.tutor_email,
			selected_course_category: props.category,
			category_value: 1,
			tutor_value: 1,
			all_admin_tutors: '',
			course_name_err_text: '',
			course_code_err_text: '',
			course_tutor_err_text: '',
		}

		this.createNewCourse = this.createNewCourse.bind(this);
		this.handleCourseNameChange = this.handleCourseNameChange.bind(this);
		this.handleCourseCodeChange = this.handleCourseCodeChange.bind(this);
	}

	componentWillMount() {
		var tutors = this.props.all_users_data.all_users_data;
		if (this.props.ctype === "edit"){
			var i;
			if ((this.props.login_logout.user_data.role === 'Admin')
					&& (this.props.all_users_data.fetch_state === data_fetch_stages.ENDED))
			{
				/* Set course tutor value */
				for (i=0; i<tutors.length; i++){
					if (tutors[i].email === this.props.tutor_email){
						break;
					}
				}
				if (i<tutors.length){
					this.setState({tutor_value: i+1});
				}
			}

			/* Set course actegory value */
			for (i=0; i<course_categories.length; i++){
				if (this.props.category === course_categories[i]){
					break;
				}
			}
			if (i<course_categories.length){
				this.setState({category_value: i+1});
			}
		}
		this.props.setBackendErrorsInForm('');
	}

	handleCourseNameChange(event) {
		this.setState({course_name_value: event.target.value});
	}

	handleCourseCodeChange(event) {
		this.setState({course_code_value: event.target.value});
	}

	handleCourseTutorChange = (event, index, value) => {
		var tutor_email = this.props.all_users_data.all_users_data[value-1].email;
		this.setState({tutor_value: value, selected_tutor_email: tutor_email});
	}

	handleCourseCategoryChange = (event, index, value) => {
		this.setState({category_value: value, selected_course_category: course_categories[value-1]});
	}

	createNewCourse(){
		const course_name = this.state.course_name_value.trim();
		const course_code = this.state.course_code_value.trim();
		var course_name_error = '';
		var course_code_error = '';
		var error_found = false;
		/* Check length */
		if (course_name.length < 5 ){
			course_name_error = 'Course name should be at least 5 characters';

			error_found = true;
		}

		if (!error_found){
			/* Check alphanumeric */
			if (!alphaNumericSpaceDash(course_name)){
				course_name_error =  'Course name should contain only alphabets, numbers, spaces and dashes';
				error_found = true;
			}
		}

		/* Check length */
		error_found = false;
		if (course_code.length < 3){
			course_code_error =  'Course code should be at least 3 characters';
			error_found = true;
		}
		if (!error_found){
			/* Check alphanumeric */
			if (!alphanumeric(course_code)){
				course_code_error =  'Course code should be alpha numeric';
				error_found = true;
			}
		}

		if (course_name_error === '' && course_code_error === ''){
			var tutor_email;
			var course_cat;
			if (this.state.selected_tutor_email === ''){
				if (this.props.login_logout.user_data.role === roles.ADMIN){
					tutor_email = this.props.all_users_data.all_users_data[0].email;
				}
				else{
					tutor_email = this.props.login_logout.user_data.email;
				}
			}
			else{
				tutor_email = this.state.selected_tutor_email;
			}

			if (this.state.selected_course_category === ''){
				course_cat = course_categories[this.state.category_value-1]
			}
			else {
				course_cat = this.state.selected_course_category;
			}
			var status = course_section_statuses.UNPUBLISHED;
			if (this.props.ctype === "edit"){
				status = this.props.status;
				this.props.createCourse(course_name, course_code, tutor_email, course_cat, status, true);
			}
			else{
				this.props.createCourse(course_name, course_code, tutor_email, course_cat, status);
			}
		}
		this.setState({
			course_name_err_text: course_name_error,
			course_code_err_text: course_code_error
		});
	}

	renderCourseCreatorListRow(user_data, index){
		index++;
		var row = <MenuItem value={index} primaryText={user_data.name} key={index}/>
		return row;
	}

	getAllUsersList(all_users_data){
		return (
      all_users_data.map((user_data, index) =>
				this.renderCourseCreatorListRow(user_data, index)
		   )
    );
	}

	renderCourseCategoryListRow(category, index){
		index++;
		var row = <MenuItem value={index} primaryText={category} key={index}/>
		return row;
	}

	getCourseCategoriesList(course_cats){
		return (
      course_cats.map((category, index) =>
				this.renderCourseCategoryListRow(category, index)
		   )
    );
	}

	render(){
		var create_button = '';
		var creator = '';
		var category = '';
		var c_code_disabled = false;
		if (this.props.ctype === "new"){
			create_button = <RaisedButton
												label="Create course"
												primary={true}
												labelStyle={ small_btn }
												onClick={ this.createNewCourse } />;
		}
		else if (this.props.ctype === "edit"){
			create_button = <RaisedButton
												label="Save"
												primary={true}
												labelStyle={ small_btn }
												onClick={ this.createNewCourse } />;
			c_code_disabled = true;
		}

		category = <div>
								<SelectField
									floatingLabelText="Course category"
									value={this.state.category_value}
									onChange={this.handleCourseCategoryChange}
								>
									{this.getCourseCategoriesList(course_categories)}
								</SelectField>
							</div>;

		if ((this.props.login_logout.user_data.role === 'Admin')
				&& (this.props.all_users_data.fetch_state === data_fetch_stages.ENDED))
		{
			creator = <div>
									<SelectField
										floatingLabelText="Tutor's Name"
										value={this.state.tutor_value}
										onChange={this.handleCourseTutorChange}
									>
										{this.getAllUsersList(this.props.all_users_data.all_users_data)}
									</SelectField>
								</div>;
		}

		return (
			<div className="create-course-wrapper">
				<MuiThemeProvider>
					<div>
						<TextField
							fullWidth={true}
							hintText="Course Name"
							inputStyle={ text_field }
							hintStyle={ hint_style }
							value={ this.state.course_name_value }
							onChange={ this.handleCourseNameChange }
							errorText={ this.state.course_name_err_text }
						/>
						<br />
						<TextField
							fullWidth={true}
							hintText="Course Code"
							inputStyle={ text_field }
							hintStyle={ hint_style }
							disabled={ c_code_disabled }
							value={ this.state.course_code_value }
							onChange={ this.handleCourseCodeChange }
							errorText={ this.state.course_code_err_text }
						/>
						<br />
						{ category }
						<br />
						{ creator }
						<br />
						<div className="content-error">{this.props.active_form_error.error_text}</div>
						<br />
						{ create_button }
						<br />
					</div>
				</MuiThemeProvider>
			</div>
		);
	}
}
function mapStateToProps(state){
  return {
		login_logout: state.login_logout,
		all_users_data: state.all_users_data,
		active_form_error: state.active_form_error,
		sidebar: state.sidebar
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
		clickSidebarItem: clickSidebarItem,
		createCourse: createCourse,
		fetchAllUsersData: fetchAllUsersData,
		setBackendErrorsInForm: setBackendErrorsInForm
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateCourse)
