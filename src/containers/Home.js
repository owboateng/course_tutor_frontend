import React from 'react';
import { Drawer, RaisedButton } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TopMenu from './TopMenu';
import Content from './Content';
import Sidebar from './Sidebar';

import { Redirect } from 'react-router';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { sidebar_items,
         data_fetch_stages
       } from '../actions/constants';
import { fetchAllCourseData,
         fetchUserCourseData,
         fetchAllUsersData,
         fetchUserArticles,
         fetchAllArticles
       } from '../actions/ajax';
import { showHideSidebarDrawer,
         clickSidebarItem
      } from '../actions/gui';
import MediaQuery from 'react-responsive';
import GoogleAd from 'react-google-ad'
import { home_url_params } from '../settings/string_constants';

import '../css/adsense.css';

const menu_butt_style = {
  marginTop: '10px',
  float: 'right',
  marginRight: '2%',
  padding: '0px'
}

class Home extends React.Component {
	constructor(props) {
		super(props);

    this.state = {
      open_drawer: false,
      redirect_to_mycourses: false,
    };

	}

  componentWillMount() {
    if (this.props.location.pathname.endsWith(home_url_params.MY_COURSES)){
      this.props.fetchUserCourseData(this.props.login_logout.user_data.email);
      if (!this.props.sidebar || this.props.sidebar.clicked !== sidebar_items.MY_COURSES){
        this.props.clickSidebarItem(sidebar_items.MY_COURSES);
      }
    }
    else if (this.props.location.pathname.endsWith(home_url_params.ALL_COURSES)){
      this.props.fetchAllCourseData();
      if (!this.props.sidebar || this.props.sidebar.clicked !== sidebar_items.ALL_COURSES){
        this.props.clickSidebarItem(sidebar_items.ALL_COURSES);
      }
    }
    else if (this.props.location.pathname.endsWith(home_url_params.MY_ARTICLES)){
      this.props.fetchUserArticles(this.props.login_logout.user_data.email);
      if (!this.props.sidebar || this.props.sidebar.clicked !== sidebar_items.MY_ARTICLES){
        this.props.clickSidebarItem(sidebar_items.MY_ARTICLES);
      }
    }
    else if (this.props.location.pathname.endsWith(home_url_params.ALL_ARTICLES)){
      this.props.fetchAllArticles();
      if (!this.props.sidebar || this.props.sidebar.clicked !== sidebar_items.ALL_ARTICLES){
        this.props.clickSidebarItem(sidebar_items.ALL_ARTICLES);
      }
    }
    else if (this.props.location.pathname.endsWith(home_url_params.CREATE_COURSE)){
      this.props.fetchAllUsersData();
      if (!this.props.sidebar || this.props.sidebar.clicked !== sidebar_items.CREATE_COURSE){
        this.props.clickSidebarItem(sidebar_items.CREATE_COURSE);
      }
    }
    else if (this.props.location.pathname.endsWith(home_url_params.CREATE_ARTICLE)){
      this.props.fetchAllUsersData();
      if (!this.props.sidebar || this.props.sidebar.clicked !== sidebar_items.CREATE_ARTICLE){
        this.props.clickSidebarItem(sidebar_items.CREATE_ARTICLE);
      }
    }
    else if (this.props.location.pathname.endsWith(home_url_params.SETTINGS)){
      if (this.props.login_logout.user_data.role === "Admin"){
        if (!this.props.sidebar || this.props.sidebar.clicked !== sidebar_items.SETTINGS){
          this.props.clickSidebarItem(sidebar_items.SETTINGS);
        }
      }
      else {
        this.setState({redirect_to_mycourses: true})
      }
    }

  }

  handleDrawerToggle = () => this.setState({open_drawer: !this.state.open_drawer});
  handleOnRequestChange = (open_drawer) => this.setState({open_drawer});

  render(){
    var ad = '';
		if (!this.props.login_logout || (this.props.login_logout.login_state !== data_fetch_stages.AUTHENTICATED)){
			return <Redirect to='/' />;
		} // Check if this works all the time
    else if (this.state.redirect_to_mycourses){
      var redirect_address = '/home/my-courses';
      return <Redirect to={redirect_address} />;
    }
    /* dont show add on local host */
    if(process.env.NODE_ENV !== 'development'){
        ad =
          <div className="ad-wrapper">
            <GoogleAd client="ca-pub-4150987432499003" slot="6205780231" format="auto" />
          </div>
    }

    return (
      <div className="">
        <MuiThemeProvider>
          <div>
            <TopMenu />
            { ad }
            <MediaQuery minDeviceWidth={769}>
              <Sidebar />
            </MediaQuery>
            <MediaQuery maxDeviceWidth={768}>
              <div>
                <RaisedButton
                  label="Menu"
                  style={menu_butt_style}
                  onTouchTap={ this.handleDrawerToggle }
                />
                <Drawer
                  docked={false}
                  width={220}
                  open={ this.state.open_drawer }
                  onRequestChange={ this.handleOnRequestChange }
                >
                  <Sidebar/>
                </Drawer>
              </div>
            </MediaQuery>
						<Content />
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
		login_logout: state.login_logout,
    sidebar_drawer: state.sidebar_drawer,
	}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    showHideSidebarDrawer: showHideSidebarDrawer,
    fetchAllCourseData: fetchAllCourseData,
		fetchUserCourseData: fetchUserCourseData,
		fetchAllUsersData: fetchAllUsersData,
    fetchUserArticles: fetchUserArticles,
    fetchAllArticles: fetchAllArticles,
    clickSidebarItem: clickSidebarItem
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
