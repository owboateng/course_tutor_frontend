import React from 'react';
import CreateCourse from './CreateCourse';

import { connect } from 'react-redux';

// const toolbarStyle = {
// 	fontSize: 12
// };

class CourseCreator extends React.Component {
	constructor(props) {
		super(props);

    this.state = {

    };
  }

  render() {
    var content_layout;

    content_layout = (
      <CreateCourse
				name=""
				code=""
				category=""
				status=""
				tutor_email=""
				ctype="new" />
    );
    return content_layout;
  }
}

function mapStateToProps(state){
  return {
    sidebar: state.sidebar
	}
}

export default connect(mapStateToProps)(CourseCreator)
