import { combineReducers } from 'redux';
import * as ActionConstants from '../actions/constants';

var initial_login_state = {
  user_data: {},
  login_state: ActionConstants.data_fetch_stages.UNTOUCHED
}
function loginLogoutRequested(state=initial_login_state, action){
  switch (action.type){
    case ActionConstants.USER_LOGGED_IN:
      return action.payload;
    default:
      return state;
  }
}

/* User and all courses */

var initial_user_course_data = {
  user_courses: {},
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function userCourseDataUpdated(state=initial_user_course_data, action) {
  switch (action.type) {
    case ActionConstants.data_items.USER_COURSE_DATA_UPDATED:
      return action.payload;
    default:
      return state;
  }
}
var initial_all_course_data = {
  all_courses: {},
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function allCourseDataUpdated(state=initial_all_course_data, action) {
  switch (action.type) {
    case ActionConstants.data_items.ALL_COURSE_DATA_UPDATED:
      return action.payload;
    default:
      return state;
  }
}
var initial_admin_data = {
  admin_data: {},
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function adminsDataUpdated(state=initial_admin_data, action) {
  switch (action.type) {
    case ActionConstants.data_items.ADMIN_DATA_UPDATED:
      return action.payload;
    default:
      return state;
  }
}
var initial_tutor_data = {
  tutor_data: {},
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function tutorsDataUpdated(state=initial_tutor_data, action) {
  switch (action.type) {
    case ActionConstants.data_items.TUTOR_DATA_UPDATED:
      return action.payload;
    default:
      return state;
  }
}

var initial_admin_tutor_state = {
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function adminTutorDeleted(state=initial_admin_tutor_state, action){
  switch (action.type) {
    case ActionConstants.data_items.ADMIN_TUTOR_DELETED:
      return action.payload;
    default:
      return state;
  }
}

var initial_all_users_data = {
  all_users_data: {},
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function allUsersDataUpdated(state=initial_all_users_data, action){
  switch (action.type) {
    case ActionConstants.data_items.ALL_USERS_DATA_UPDATED:
      return action.payload;
    default:
      return state;
  }
}
var initial_sections_data = {
  course_sections: {},
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function courseSectionsDataUpdated(state=initial_sections_data, action){
  switch (action.type) {
    case ActionConstants.data_items.COURSE_SECTIONS_DATA_UPDATED:
      return action.payload;
    default:
      return state;
  }
}

var initial_section_state = {
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function courseSectionDeleted(state=initial_section_state, action){
  switch (action.type) {
    case ActionConstants.data_items.COURSE_SECTION_DELETED:
      return action.payload;
    default:
      return state;
  }
}

var initial_course_state = {
  fetch_state: ActionConstants.data_fetch_stages.STARTED
}
function courseDeleted(state=initial_course_state, action){
  switch (action.type) {
    case ActionConstants.data_items.COURSE_DELETED:
      return action.payload;
    default:
      return state;
  }
}

/* Articles */
var user_articles_state = {
  user_articles: [],
  stage: ActionConstants.data_fetch_stages.STARTED
}
function userArticlesUpdated(state=user_articles_state, action){
  switch (action.type) {
    case ActionConstants.data_items.USER_ARTICLES_UPDATE:
      return action.payload;
    default:
      return state;
  }
}

var all_articles_state = {
  all_articles: [],
  stage: ActionConstants.data_fetch_stages.STARTED
}
function allArticlesUpdated(state=all_articles_state, action){
  switch (action.type) {
    case ActionConstants.data_items.ALL_ARTICLES_UPDATE:
      return action.payload;
    default:
      return state;
  }
}

/* UI */
var initial_sidebar_state = {
  clicked: ActionConstants.sidebar_items.MY_COURSES,
  item_num: 1,
  is_first_click: true
}

function sidebarItemSelected(state=initial_sidebar_state, action) {
  switch (action.type) {
    case ActionConstants.SIDEBAR_ITEM_CLICKED:
      return action.payload;
    default:
      return state;
  }
}

var initial_settings_sidebar_state = {
  clicked: ActionConstants.settings_sidebar_items.ADMINISTRATORS,
  item_num: 1,
}

function settingsSidebarItemSelected(state=initial_settings_sidebar_state, action) {
  switch (action.type) {
    case ActionConstants.SETTINGS_SIDEBAR_ITEM_CLICKED:
      return action.payload;
    default:
      return state;
  }
}

var initial_dialog_state = { active_dialog: ActionConstants.dialog_names.NONE};

function closeOpenDialogRequested(state=initial_dialog_state, action){
   switch(action.type) {
    case ActionConstants.CLOSE_OPEN_DIALOG:
      return action.payload;
    default:
      return state;
  }
}

var initial_snackbar_state = { is_on: false, text: ''};
function closeOpenSnackBarRequested(state=initial_snackbar_state, action){
   switch(action.type) {
    case ActionConstants.CLOSE_OPEN_SNACKBAR:
      return action.payload;
    default:
      return state;
  }
}

var initial_form_err_text = {error_text: ''};
function setFormErrorText(state=initial_form_err_text, action){
  switch(action.type) {
    case ActionConstants.data_items.FORM_ERROR_TEXT_SET:
      return action.payload;
    default:
      return state;
  }
}

var initial_drawer_state = { is_shown: false, is_first_click: false };
function showHideSidebarDrawerRequested(state=initial_drawer_state, action){
   switch(action.type) {
    case ActionConstants.SHOW_HIDE_SIDEBAR_DRAWER:
      return action.payload;
    default:
      return state;
  }
}

/* UI ended */

const courseTutorApp = combineReducers({
    sidebar: sidebarItemSelected,
    settings_sidebar: settingsSidebarItemSelected,
    login_logout: loginLogoutRequested,
    user_course_data: userCourseDataUpdated,
    all_course_data: allCourseDataUpdated,
    administrator_data: adminsDataUpdated,
    tutor_data: tutorsDataUpdated,
    all_users_data: allUsersDataUpdated,
    course_sections_data: courseSectionsDataUpdated,
    user_articles: userArticlesUpdated,
    all_articles: allArticlesUpdated,
    active_dialog_data: closeOpenDialogRequested,
    active_snackbar: closeOpenSnackBarRequested,
    active_form_error: setFormErrorText,
    course_section_delete: courseSectionDeleted,
    course_delete: courseDeleted,
    sidebar_drawer: showHideSidebarDrawerRequested,
    admin_tutor_delete: adminTutorDeleted
  }
);

export default courseTutorApp;
