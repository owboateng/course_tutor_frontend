/**
*Function to check letters, numbers, spaces and dashes
*/
function alphaNumericSpaceDash(inputtxt)
{
  var letterNumber = /^[0-9a-zA-Z -]+$/;
  if(inputtxt.match(letterNumber))
  {
    return true;
  }
  else
  {
    return false;
  }
}
/**
*Function to check letters, numbers, spaces
*/
function alphanumeric(inputtxt)
{
  var letterNumber = /^[0-9a-zA-Z ]+$/;
  if(inputtxt.match(letterNumber))
  {
    return true;
  }
  else
  {
    return false;
  }
}

/**
*Function to check letters
*/
function alpha(inputtxt)
{
  var letter = /^[a-zA-Z ]+$/;
  if(inputtxt.match(letter))
  {
    return true;
  }
  else
  {
    return false;
  }
}

function validate_pass(pwd){
  if(pwd.length < 6) {
    return ("Password must be at least 6 characters!");
  }
  var re = /[0-9]/;
  if(!re.test(pwd)) {
    return ("password must contain at least one number (0-9)!");
  }
  re = /[a-z]/;
  if(!re.test(pwd)) {
    return ("password must contain at least one lowercase letter (a-z)!");
  }
  re = /[A-Z]/;
  if(!re.test(pwd)) {
    return ("password must contain at least one uppercase letter (A-Z)!");
  }
  return '';
}

function validateEmail(mail)
{
  if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/.test(mail))
  {
    return '';
  }

  return ("You have entered an invalid email address!");
}

function format_article_title(title){
  if (title.length > 35){
    title = title.substring(0, 35) + '...';
  }
  return title;
}

export { alphanumeric, alpha, alphaNumericSpaceDash, validate_pass, validateEmail,
         format_article_title };
