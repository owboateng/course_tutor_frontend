const iconstyle = {
	fill: "#51CBFC"
}
const listitemstyle = {
	color: "#03347D",
	fontSize: "12px"
}
const innertextstyle = {
	paddingLeft: "42px"
}
const sidebar_first_style = {
	marginTop: "20px"
}

const noBorder = {
  border: "None"
}

export { iconstyle, listitemstyle,
         innertextstyle, sidebar_first_style,
         noBorder
       };
