import 'string.prototype.endswith';
import 'string.prototype.startswith';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/custom/App';
import courseTutorApp from './reducers/reducers';

import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';
import { persistStore, autoRehydrate } from 'redux-persist';
// import reduxReset from 'redux-reset';
import injectTapEventPlugin from 'react-tap-event-plugin';

import './css/index.css';

const store = compose(autoRehydrate(), applyMiddleware(thunk))(createStore)(courseTutorApp);

injectTapEventPlugin();
class AppProvider extends React.Component {

  constructor() {
    super()
    this.state = { rehydrated: false }
  }

  componentWillMount(){
    persistStore(store, {}, () => {
      this.setState({ rehydrated: true })
    })
  }

  render() {
    if(!this.state.rehydrated){
      return <div>Loading...</div>
    }
    return (
      <Provider store={store}>
        <App />
      </Provider>
    )
  }
}

ReactDOM.render(
  <AppProvider/>,  document.getElementById('root')
);
