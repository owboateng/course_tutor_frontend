export const roles = {
  ADMIN: "Admin",
  TUTOR: "Tutor"
}

export const course_section_statuses = {
  UNPUBLISHED: 'Unpublished',
  PUBLISHED: 'Published',
  ARCHIVED: 'Archived'
}

export const home_url_params = {
  MY_COURSES: 'my-courses',
  ALL_COURSES: 'all-courses',
  MY_ARTICLES: 'my-articles',
  ALL_ARTICLES: 'all-articles',
  CREATE_COURSE: 'create-course',
  CREATE_ARTICLE: 'create-article',
  SETTINGS: 'settings'
}
