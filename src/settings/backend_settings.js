const backend_base_urls = {
  LOCALHOST: 'http://localhost:8000/backend/',
  REMOTE_TEST: 'https://tutor.boeskbackends.com/backend/'
}

function backend_base_URL(){
  var base_url = '';
  if(process.env.NODE_ENV === 'development'){
    base_url = backend_base_urls.LOCALHOST;
  }
  else{
    base_url = backend_base_urls.REMOTE_TEST;
  }

  return base_url;
}

export const backend_base_url = backend_base_URL();

export const admin_email = "admin@tutor.boesk.com";
